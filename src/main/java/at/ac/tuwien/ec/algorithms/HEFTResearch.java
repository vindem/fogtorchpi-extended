package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import at.ac.tuwien.ec.algorithms.utils.NodeRankComparator;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class HeftResearch extends DAGResearch {
	
	public HeftResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
		
	}
	
	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(A.S.size(), new NodeRankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		
		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0.0;
		int scheduledTasks = 0;
		//deploying root node
		//MobileSoftwareComponent root = tasks.poll();
		//deploy(deployment,root,I.getMobileDevice("mobile_0"));
		scheduledTasks++;
		MobileSoftwareComponent currTask;
		while(!(currTask = tasks.poll()).getId().equals("sink")){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
				scheduledTasks++;
				//System.out.println("Scheduled nodes: "+scheduledTasks+" out of "+A.S.size());
			}
			double tMin = Double.MAX_VALUE;
			ComputationalNode target = null;
			//boolean canSchedule = true;
			//while(canSchedule && !tasks.isEmpty())
			//{
				//MobileSoftwareComponent currTask = tasks.poll();
				if(currTask.getId().equals("root"))
				//{
					//deploy(deployment,currTask,I.getMobileDevice("mobile_0"));
					continue;
				//}
					
				if(!currTask.isOffloadable())
					if(isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
					{
						target = I.getMobileDevice(currTask.getUid());
						scheduledNodes.add(currTask);
						//tasks.remove();
					}
					else
					{
						if(scheduledNodes.isEmpty())
							return null;
					}
				else{
					double maxP = Double.MIN_VALUE;
					for(MobileSoftwareComponent cmp : A.getPredecessors(currTask))
						if(cmp.getRuntime() > maxP)
							maxP = cmp.getRuntime();
					for(CloudDatacentre cdc : I.C.values())
						if(maxP + currTask.getRuntimeOnNode(cdc, I) < tMin
								&& isValid(deployment, currTask, cdc))
						{
							tMin = maxP + currTask.getRuntimeOnNode(cdc, I);
							target = cdc;
						}
					for(FogNode fn : I.F.values())
						if(maxP + currTask.getRuntimeOnNode(fn, I) < tMin
								&& isValid(deployment, currTask, fn))
						{
							tMin = maxP + currTask.getRuntimeOnNode(fn, I);
							target = fn;
						}
					if(maxP + currTask.getRuntimeOnNode(I.getMobileDevice(currTask.getUid()), I) < tMin
							&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
						target = I.getMobileDevice(currTask.getUid());
				}
				if(target!=null)
				{
					
					deploy(deployment,currTask,target);
					setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
					scheduledNodes.add(currTask);
					
				}
				else
				{
					if(scheduledNodes.isEmpty())
						return null;
					//else
						//canSchedule = false;
				}
			//}
		}
		//deployment.runTime += currentRuntime;
		while(!scheduledNodes.isEmpty())
		{
			MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
			currentRuntime = firstTaskToTerminate.getRuntime();
			deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
			scheduledNodes.remove(firstTaskToTerminate);
		}
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		deployments.add(deployment);
		return deployments;
	}
	
	
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
