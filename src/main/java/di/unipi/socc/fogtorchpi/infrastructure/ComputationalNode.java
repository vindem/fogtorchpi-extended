/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.unipi.socc.fogtorchpi.infrastructure;

import di.unipi.socc.fogtorchpi.application.ExactThing;
import java.util.ArrayList;
import java.util.Collection;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.utils.Coordinates;
import di.unipi.socc.fogtorchpi.utils.Cost;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import di.unipi.socc.fogtorchpi.utils.Software;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.NETEnergyModel;

/**
 *
 * @author stefano
 */
public abstract class ComputationalNode implements Comparable {

    private double MIPS_PER_CORE = 1;
	private String identifier;
    private HashMap<String, Software> software;
    private Coordinates coords;
    private Hardware hw;
    public double heuristic;
    private boolean keepLight;
    private CPUEnergyModel cpuEnergyModel;
    private NETEnergyModel netEnergyModel;
	protected boolean isMobile;
	/*cost calculated assuming a price of 0.10$ x KwH
	* 0.10/KwH / (1000 joule/Kwsec * 60sec/min * 60 min/hour)
	*/
	final double costPerJoule = 2.8*10e-8;

    public ComputationalNode() {
    	
    }

    public ComputationalNode(CPUEnergyModel energyModel){
    	this.cpuEnergyModel = energyModel;
    	isMobile = false;
    }
    
    public void setSoftware(Collection<Couple<String, Double>> software) {
        this.software = new HashMap<>();
        for (Couple s : software) {
            Software value = new Software(((Couple) s).getA().toString(), (double) ((Couple) s).getB());
            this.software.put(((Couple) s).getA().toString(), value);
        }
    }

    protected void setSoftware(HashMap<String, Software> software){
    	this.software = software;
    }
    
    protected void setCoordinates(Coordinates coords){
    	this.coords = coords;
    }
    
    public void setHardware(Hardware h) {
        setHw(new Hardware(h));
    }

    public Hardware getHardware() {
        return getHw();
    }

    public HashMap<String, Software> getSoftware() {
        return this.software;
    }

    public void setKeepLight(boolean kl) {
        this.keepLight = kl;
    }

    public boolean getKeepLight() {
        return this.keepLight;
    }

    public String getId() {
        return this.identifier;
    }

    public void setId(String identifier) {
        this.identifier = identifier;
    }

    public void setCoordinates(double x, double y) {
        this.coords = new Coordinates(x, y);
    }

    public Coordinates getCoordinates() {
        return this.coords;
    }

    public double distance(ComputationalNode n) {
        return getCoordinates().distance(n.getCoordinates());
    }

    public abstract boolean isCompatible(SoftwareComponent s);

    public abstract void deploy(SoftwareComponent s);

    public abstract void undeploy(SoftwareComponent s);
    
    public abstract Cost computeCost(SoftwareComponent s, Infrastructure I);

    public abstract double computeHeuristic(SoftwareComponent s); //, Coordinates deploymentLocation

    @Override
    public boolean equals(Object o) {
        ComputationalNode n = (ComputationalNode) o;
        return n.identifier.equals(this.identifier);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.identifier);
        hash = 73 * hash + Objects.hashCode(this.software);
        hash = 73 * hash + Objects.hashCode(this.coords);
        return hash;
    }

    public boolean isReachable(String t, Infrastructure I, QoSProfile qNodeThing, QoSProfile qThingNode) {
        boolean reach = false;

        if (I.L.containsKey(new Couple<String, String>(this.getId(), t))) {

            QoSProfile q1 = I.L.get(new Couple(this.getId(), t)); //nodeThing
            QoSProfile q2 = I.L.get(new Couple(t, this.getId())); //thingNode
            reach = q1.supports(qNodeThing) && q2.supports(qThingNode);
        }

        return reach;
    }

    @Override
    public int compareTo(Object o) {
        ComputationalNode s2 = (ComputationalNode) o;
        return Double.compare(s2.heuristic, this.heuristic);
    }

	public CPUEnergyModel getCPUEnergyModel() {
		return this.cpuEnergyModel;
	}

	public void setCPUEnergyModel(CPUEnergyModel cpuModel) {
		this.cpuEnergyModel = cpuModel;		
	}

	public void setMipsPerCore(double mips){
		this.MIPS_PER_CORE = mips;
	}
	
	public double getMipsPerCore() {
		return this.MIPS_PER_CORE;
	}

	public boolean isMobile() {
		return isMobile;
	}

	public NETEnergyModel getNetEnergyModel() {
		return netEnergyModel;
	}

	public void setNetEnergyModel(NETEnergyModel netEnergyModel) {
		this.netEnergyModel = netEnergyModel;
	}

	public double getEnergyCost() {
		return costPerJoule;
	}

	public Hardware getHw() {
		return hw;
	}

	public void setHw(Hardware hw) {
		this.hw = hw;
	}
	
	
	public abstract boolean isCompatible(MobileSoftwareComponent s, MobileCloudInfrastructure i);

}
