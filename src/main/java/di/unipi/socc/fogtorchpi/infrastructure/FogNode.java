package di.unipi.socc.fogtorchpi.infrastructure;

import di.unipi.socc.fogtorchpi.application.ExactThing;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.deployment.MonteCarloSimulation;
import di.unipi.socc.fogtorchpi.utils.Constants;
import di.unipi.socc.fogtorchpi.utils.Cost;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import di.unipi.socc.fogtorchpi.utils.Software;

/**
 *
 * @author stefano
 */
public class FogNode extends ComputationalNode{
    public HashSet<String> connectedThings;
    HashMap<String, Hardware> virtualMachines;
    
    public HashMap<String, Hardware> getVirtualMachines() {
		return virtualMachines;
	}

    
    public FogNode(String identifier, Collection<Couple<String, Double>> software, Hardware hw, double x, double y){
        super.setId(identifier);
        super.setHardware(hw);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        connectedThings = new HashSet<>();
        super.setKeepLight(false);
        virtualMachines =  new HashMap<>();
    }

    public FogNode(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        connectedThings = new HashSet<>();
        this.virtualMachines = new HashMap<>();
        
        for ( Couple<String,Double> c : vmTypes )
            this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
     }
    
    public FogNode(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes, CPUEnergyModel cpuModel){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        super.setCPUEnergyModel(cpuModel);
        connectedThings = new HashSet<>();
        this.virtualMachines = new HashMap<>();
        
        for ( Couple<String,Double> c : vmTypes )
            this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
     }
    
    public FogNode(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes, CPUEnergyModel cpuModel, double mipsPerCore){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        super.setCPUEnergyModel(cpuModel);
        super.setMipsPerCore(mipsPerCore);
        connectedThings = new HashSet<>();
        this.virtualMachines = new HashMap<>();
        
        if(vmTypes!=null)
        {
        	for ( Couple<String,Double> c : vmTypes )
        		this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
        }
    }
    
    
    @Override
    public boolean isCompatible(MobileSoftwareComponent component,MobileCloudInfrastructure I)
    {
    	return isCompatible(component);
    }
    
    public boolean isCompatible(SoftwareComponent component){
        Hardware hardwareRequest = component.getHardwareRequirements();
        Collection<Software> softwareRequest = component.getSoftwareRequirements();
        
        return super.getHardware().supports(hardwareRequest) && 
                softwareRequest.stream().noneMatch(
                        (s) -> (!super.getSoftware().containsValue(s))
                );
    }
   

    @Override
    public void deploy(SoftwareComponent s) {
        super.getHardware().deploy(s.getHardwareRequirements());
    }

    @Override
    public void undeploy(SoftwareComponent s) {
        super.getHardware().undeploy(s.getHardwareRequirements());
    }
    
        @Override
    public String toString(){
        String result = "<";
        result = result + getId() + ", " + super.getSoftware() + ", "+ super.getHardware() +", "+this.getCoordinates();        
        result += ">";
        return result; 
    }


    public double distance(Thing t) {
        return t.getCoordinates().distance(super.getCoordinates());
    }
    
    /**
     * Returns a couple with the percentage of used RAM and storage at
     * this Fog node.
     * @param d a deployment
     * @return a couple with the percentage of used RAM and storage.
     */
    public Couple<Double, Double> consumedResources(SoftwareComponent s){
        Couple<Double,Double> result = new Couple<>(0.0,0.0);
  
        Hardware used = s.getHardwareRequirements();
        result.setA(result.getA() + used.ram);
        result.setB(result.getB() + used.storage);
        
        return result;
    }

    @Override
    public double computeHeuristic(SoftwareComponent s) { //Coordinates deploymentLocation
        
        this.heuristic = super.getHardware().cores/Constants.MAX_CORES + 
                super.getHardware().ram/Constants.MAX_RAM + 
                super.getHardware().storage/Constants.MAX_HDD; //+ 1/(deploymentLocation.distance(this.getCoordinates()));
        
        if (this.getKeepLight()){
            heuristic = heuristic - 4;
        }

        return heuristic;
    }
    
    @Override
    public Cost computeCost(SoftwareComponent s, Infrastructure I) {
    	double cost = 0.0;
    	Hardware sHardwareReqs = s.getHardwareRequirements();
        
    	if (sHardwareReqs.stringDescribed && !this.virtualMachines.isEmpty())
    		cost += this.virtualMachines.get(sHardwareReqs.vmType).getMonthlyCost(s);
    	else
    	{
    		cost += this.getHardware().getMonthlyCost(s) * s.getRuntimeOnNode(this, (MobileCloudInfrastructure) I);
    		
    	}
    	for (Software soft : s.getSoftwareRequirements()) 
    		cost += super.getSoftware().get(soft.getName()).getCost();
    	

    	for (ThingRequirement thing : s.getThingsRequirements()) 
    		cost += ((ExactThing) thing).getMonthlyInvoke() * I.T.get(((ExactThing) thing).getId()).getMonthlyCost(s);
    	
    	cost += getFogPenalty(s,(MobileCloudInfrastructure) I);    	
    	
   		return new Cost(cost);
   	}

    private double getFogPenalty(SoftwareComponent s, MobileCloudInfrastructure i){
    	/*Hardware sHardwareReqs = s.getHardwareRequirements();
    	
    	this.virtualMachines.get(sHardwareReqs.vmType)*/
    	Hardware sHardwareReqs = s.getHardwareRequirements();
    	//double cloudCost = this.virtualMachines.get(sHardwareReqs.vmType).getMonthlyCost(s);
    	//double cloudCost = this.virtualMachines.get("xlarge").getMonthlyCost(s);
    	double instCloudCost = this.getHardware().getMonthlyCost(s);
    	double minCloudTime = Double.MAX_VALUE;
    	double minEdgeTime = Double.MAX_VALUE;
    	double minEdgeLatency = Double.MAX_VALUE;
    	double minCloudLatency = Double.MAX_VALUE;
    	
    	for(Entry<Couple, QoSProfile> entry: i.L.entrySet()){
    		Couple c = entry.getKey();
    		if(c.getA().equals(i.getMobileDevice().getId())){
    			String id = (String) c.getB();
    			if(i.isFogNode(id))
    				if(entry.getValue().getLatency() < minEdgeLatency)
    					minEdgeLatency = entry.getValue().getLatency();
    			if(i.isCloudNode(id))
    				if(entry.getValue().getLatency() < minCloudLatency)
    					minCloudLatency = entry.getValue().getLatency();
    		}
    	}
    	
    	for(CloudDatacentre c : i.C.values()){
    		double tmp = s.getRuntimeOnNode(c, i);
    		if(tmp < minCloudTime)
    			minCloudTime = tmp;
    	}
    	
    	for(FogNode f : i.F.values()){
    		double tmp = s.getRuntimeOnNode(f, i);
    		if(tmp < minEdgeTime)
    			minEdgeTime = tmp;
    	}
    	
    	
    	double cloudCost = instCloudCost * minCloudTime;
    	//average cloud latency + average service rate on cloud - average latency cloud
    	//double timeFactor =  70.0 + 1.0/64.0 - 54.0;
    	double timeFactor =  200.0 + (1.0/64.0) - 54.0;
    	
    	double penalty = (timeFactor / SimulationConstants.Eta) 
    			- Math.sqrt((SimulationConstants.Eta * cloudCost + timeFactor) / 
    					(Math.pow(SimulationConstants.Eta, 2.0) * 8.0) );
    	
    	return penalty/1000;
    }
    	
}


