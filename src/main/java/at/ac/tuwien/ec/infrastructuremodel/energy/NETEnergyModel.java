package at.ac.tuwien.ec.infrastructuremodel.energy;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;

public interface NETEnergyModel {
	
	public double computeNETEnergy(SoftwareComponent s, ComputationalNode n, Infrastructure i);

}
