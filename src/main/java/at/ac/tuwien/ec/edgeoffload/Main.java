/**
 * Original file of paper revised according to FT refactoring with cost.
 */
package at.ac.tuwien.ec.edgeoffload;


import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.MonteCarloSimulation;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.util.Arrays.asList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Handler;
import java.util.logging.Level;

import org.uma.jmetal.util.JMetalLogger;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.utils.DeploymentsHistogram;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import at.ac.tuwien.ec.utils.MontecarloStatisticsPrinter;

/**
 *
 * @author Stefano
 */
public class Main {
    
	private static Helper helper;
	
	public static void main(String[] args) {
		processArgs(args);
		helper = new Helper();
		Handler[] handlers = JMetalLogger.logger.getHandlers();
    	for(Handler hand : handlers)
    		JMetalLogger.logger.removeHandler(hand);
    	
    	SimulationConstants.logger.setLevel(SimulationConstants.logLevel);
		JMetalLogger.logger.setLevel(Level.OFF);
      	
		long totStartTime = 0, totEndTime = 0;
		
		long iStartTime = 0, iEndTime = 0;
		iStartTime = System.currentTimeMillis();
		MobileCloudInfrastructure I = helper.setupInfrastructure();
		iEndTime = System.currentTimeMillis();
		System.out.println("Infrastructure set up in secs: " + (iEndTime - iStartTime)/1000.0);
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd-HH_mm_ss");
		Date date = new Date();

		HashMap<String,DeploymentsHistogram> histograms;
		//System.out.println(I);
		PrintWriter[] writers = new PrintWriter[SimulationConstants.algorithms.length];
		
		for(String alg : SimulationConstants.algorithms)
		{
			File path = new File(SimulationConstants.outfile + alg + "/");
			if(!path.exists())
				path.mkdirs();
		}
		for(int i = 0; i < SimulationConstants.algorithms.length; i++){
			String filename = SimulationConstants.outfile
					+ SimulationConstants.algorithms[i] +"/"
					+ dateFormat.format(date)
					+ "-" + SimulationConstants.MAP_M
					+ "X"
					+ SimulationConstants.MAP_N
					+ "-edge-planning="
					+ SimulationConstants.edgePlanningAlgorithm
					+ "-" + SimulationConstants.targetApp
					+ "-lambdaLatency=" + SimulationConstants.lambdaLatency
					+ "-CLOUD=" + I.C.size()
					+ "-EDGE=" + I.F.size()
					+ "-" + selectAppArguments(SimulationConstants.targetApp)
					+ "-" + SimulationConstants.algorithms[i]
							+ ((SimulationConstants.algorithms[i].equals("weighted"))? 
									"-alpha="+SimulationConstants.alpha+"-beta="+SimulationConstants.beta+"-gamma="+SimulationConstants.gamma
									: "") 
							+ ((SimulationConstants.cloudOnly)? "-ONLYCLOUD":
								"-eta-" + SimulationConstants.Eta)
							+ ".data";
			try {
				writers[i] = new PrintWriter(filename,"UTF-8");
				writers[i].println(MontecarloStatisticsPrinter.getHeader());
			} 
			catch (FileNotFoundException | UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		HashMap<String,ArrayList<MobileApplication>> infWorkload = 
				new HashMap<String,ArrayList<MobileApplication>>();
		for(int i = 0; i < SimulationConstants.appExecutions.length; i++)
		{
			long wStartTime = System.currentTimeMillis();
			for(String mId: I.M.keySet())
			{
				infWorkload.put(mId,helper.setupWorkload(SimulationConstants.appExecutions[i], mId));
				System.out.println("Workload setup for "+mId + " completed.");
			}			
			//ArrayList<MobileApplication> workload = Helper.setupWorkload(1);
			long wEndTime = System.currentTimeMillis();
			System.out.println("Workload initialized in secs: " + (wEndTime - wStartTime)/1000.0);
			MonteCarloSimulation s = new MonteCarloSimulation(SimulationConstants.simulation_runs, infWorkload, I);
			totStartTime = System.currentTimeMillis();
			histograms = s.startSimulation(asList());
			totEndTime = System.currentTimeMillis();
			//SimulationConstants.offloadable_part_repetitions = offloadableParts[i];
			SimulationConstants.offloadable_part_repetitions = 1;
			for(int j = 0; j < SimulationConstants.algorithms.length; j++)
			{
				writers[j].println("Algorithm: " + SimulationConstants.algorithms[j]);
				double totQoS = 0.0;
				double maxFreq = Double.MIN_VALUE;
				Deployment mostFrequent = null;
				double avgRuntime = 0.0, avgCost = 0.0, avgBatt = 0.0;
				try {
					if(!SimulationConstants.batch)
						System.out.println(MontecarloStatisticsPrinter.getHeader());
					int k = 0;
					for (Deployment dep : histograms.get(SimulationConstants.algorithms[j]).keySet()) {
						//if(SimulationConstants.algorithms[j].equals("nsgaIII"))
							writers[j].print(MontecarloStatisticsPrinter.getStatistics(SimulationConstants.appExecutions[i], dep,I, histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
						/*else{
							double currFreq = histograms.get(SimulationConstants.algorithms[j]).getFrequency(dep);
							if(currFreq > maxFreq){
								maxFreq = currFreq;
								mostFrequent = (Deployment) dep.clone();
							}
						}*/
						avgRuntime += histograms.get(SimulationConstants.algorithms[j]).getAverageRuntime(dep);
						avgCost += histograms.get(SimulationConstants.algorithms[j]).getAverageCost(dep);
						avgBatt += histograms.get(SimulationConstants.algorithms[j]).getAverageBattery(dep);
						totQoS += 100.0 * (histograms.get(SimulationConstants.algorithms[j]).getFrequency(dep) / SimulationConstants.simulation_runs);
						if(!SimulationConstants.batch)
							//System.out.print(k + " - " + MontecarloStatisticsPrinter.getStatistics(SimulationConstants.appExecutions[i], dep, histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
							System.out.print(k + " - " + MontecarloStatisticsPrinter.getStatistics(SimulationConstants.appExecutions[i], dep, I,histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
						k++;
					}
					//if(!SimulationConstants.algorithms[j].equals("nsgaIII") && mostFrequent != null)
						//writers[j].print(MontecarloStatisticsPrinter.getStatistics(SimulationConstants.appExecutions[i], mostFrequent, histograms.get(SimulationConstants.algorithms[j]), SimulationConstants.simulation_runs));
					avgRuntime = avgRuntime / k;
					avgCost = avgCost / k;
					avgBatt = avgBatt / k;
					writers[j].println("\t AVERAGE:" + "\t \t" + avgRuntime + "\t" + avgCost + "\t" + avgBatt);
				} catch (Exception e) {
					e.printStackTrace();
				}
				writers[j].println("TOTAL QoS Satisfaction percentage: " + totQoS);
			}
		}
		for(PrintWriter p: writers) {
			p.flush();
			p.close();
		}
		System.out.println("WALL CLOCK:" + (totEndTime - totStartTime)/1000.0);
	}


	private static String selectAppArguments(String targetApp) {
		String tmp = "";
		switch(targetApp){
		case "NAVI": 
			tmp+="maps_size="+SimulationConstants.maps_size;
			break;
		case "ANTIVIRUS":
			tmp+="file_size="+SimulationConstants.file_size;
			break;
		case "FACEREC":
			tmp+="image_size="+SimulationConstants.image_size;
			break;
		case "CHESS":
			tmp+="chess_mi="+SimulationConstants.chess_mi;
			break;
		case "FACEBOOK":
			tmp+="image_size="+SimulationConstants.image_size;
			break;
		}
		return tmp;
	}


	private static void processArgs(String[] args) {
		for(String s : args)
		{
			if(s.startsWith("-mapM="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.MAP_M = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-mapN="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.MAP_N = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-edgePlanning="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.edgePlanningAlgorithm = tmp[1];
				continue;
			}
			if(s.startsWith("-mobile="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.mobileNum = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-traceIn="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.traceFile = tmp[1];
				continue;
			}
			if(s.startsWith("-outfile="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.outfile = tmp[1];
				continue;
			}
			if(s.startsWith("-iter="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.simulation_runs = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-battery="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.batteryBudget = Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-cloud="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.cloudNodes = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-edge="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.edgeNodes = Integer.parseInt(tmp[1]);
				continue;
			}
			if(s.startsWith("-wl-runs=")){
				String[] tmp = s.split("=");
				String[] input = tmp[1].split(",");
				int[] wlRuns = new int[input.length];
				for(int i = 0; i < input.length; i++)
					wlRuns[i] = Integer.parseInt(input[i]);
				SimulationConstants.appExecutions = wlRuns;
				continue;
			}
			if(s.equals("-batch"))
			{
				SimulationConstants.batch = true;
				continue;
			}
			if(s.startsWith("-map-size="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.maps_size = 1/(Double.parseDouble(tmp[1]) * 1e3);
				continue;
			}
			if(s.startsWith("-file-size="))
			{
				String[] tmp = s.split("=");
				// 1/input, to be used for lambda of exponential distribution
				SimulationConstants.file_size = 1/(Double.parseDouble(tmp[1]) * 1e3);
				continue;
			}
			if(s.startsWith("-image-size="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.image_size = 1/(Double.parseDouble(tmp[1]) * 1e3);
				continue;
			}
			if(s.startsWith("-latency="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.lambdaLatency = (Double.parseDouble(tmp[1]));
				continue;
			}
			if(s.startsWith("-chess-mi="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.chess_mi = (1.0/Double.parseDouble(tmp[1]));
				continue;
			}
			if(s.startsWith("-alpha="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.alpha = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-beta="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.beta = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-gamma="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.gamma = Double.parseDouble(tmp[1]);
			}
			if(s.startsWith("-eta="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.Eta = Double.parseDouble(tmp[1]);
				continue;
			}
			if(s.startsWith("-app="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.targetApp = tmp[1];
				continue;
			}
			if(s.startsWith("-algo="))
			{
				String[] tmp = s.split("=");
				SimulationConstants.algorithms = tmp[1].split(",");
				continue;
			}
			if(s.equals("-cloudonly"))
				SimulationConstants.cloudOnly = true;
		}
	}


	private static Deployment getLowestScoreDeployment(DeploymentsHistogram histogram, double minRuntime,
			double maxBattery, double minUserCost) {
		double minScore = Double.MAX_VALUE;
		double alpha = 1.0, beta = 1.0, gamma = 1.0;
		Deployment target = null;
		
		for(Deployment d: histogram.keySet()){
			double currRuntime = d.runTime;
			double currCost = d.deploymentMonthlyCost.getCost();
			double currBattery = d.mobileEnergyBudget;
		
			double tmpScore =  alpha * (currRuntime - minRuntime) + beta * (currCost - minUserCost) + gamma * (maxBattery - currBattery);
			if(tmpScore < minScore)
				target = d;
		}
		return target;
	}
}
