package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class NavigatorAppExp extends MobileApplication {

	public double data_variance = 1e+3;
	
	double config_panel_mips = 1.0e3;
    double gps_mips = 1.0e3;
    double control_mips = 15e3;
    double maps_mips = 15.0e3;
    double path_calc_mips = 20.0e3;
    double traffic_mips = 15.0e3;
    double voice_synth_mips = 15e3;
    double gui_mips = 5e3;
    double speed_mips = 5e3;
    
    double maps_size = SimulationConstants.maps_size;
	
	public NavigatorAppExp(){
        super(0);
		sampleTasks();
        sampleLinks();
	}

	public NavigatorAppExp(int workload_id) {
		super(workload_id);
		sampleTasks();
		sampleLinks();
	}
	
	public NavigatorAppExp(int workload_id,String mobileId) {
		super(workload_id,mobileId);
		sampleTasks();
		sampleLinks();
	}

		
	public void sampleLinks() {
		addLink("CONF_PANEL"+"_"+getWorkloadId()+","+mobileId, "GPS"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				Double.MIN_VALUE);
        addLink("CONF_PANEL"+"_"+getWorkloadId()+","+mobileId, "CONTROL"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE);
        addLink("GPS"+"_"+getWorkloadId()+","+mobileId, "CONTROL"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE);
        addLink("CONTROL"+"_"+getWorkloadId()+","+mobileId, "MAPS"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE);
        addLink("CONTROL"+"_"+getWorkloadId()+","+mobileId, "PATH_CALC"+"_"+getWorkloadId(),
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("CONTROL"+"_"+getWorkloadId()+","+mobileId, "TRAFFIC"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("MAPS"+"_"+getWorkloadId()+","+mobileId, "PATH_CALC"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("TRAFFIC"+"_"+getWorkloadId()+","+mobileId, "PATH_CALC"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("PATH_CALC"+"_"+getWorkloadId()+","+mobileId, "VOICE_SYNTH"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("PATH_CALC"+"_"+getWorkloadId()+","+mobileId, "GUI"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("PATH_CALC"+"_"+getWorkloadId()+","+mobileId, "SPEED_TRAP"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("VOICE_SYNTH"+"_"+getWorkloadId()+","+mobileId, "GUI"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
        addLink("SPEED_TRAP"+"_"+getWorkloadId()+","+mobileId, "GUI"+"_"+getWorkloadId()+","+mobileId,
        		sampleLatency(),
        		Double.MIN_VALUE,
        		Double.MIN_VALUE);
	}

	public void sampleTasks() {
		//double data_size = maps_size + ExponentialDistributionGenerator.getNext(maps_size);
		double data_size = maps_size;
		addComponent("CONF_PANEL"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python")
				,new Hardware(1, 0.1, 1)
				,false
				//,2.0 + ExponentialDistributionGenerator.getNext(config_panel_mips)*1e-1
        		,2.0e3*SimulationConstants.task_multiplier
				,5e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
        addComponent("GPS"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 0.5, 1)
        		,false
        		//,2.0 + ExponentialDistributionGenerator.getNext(gps_mips)*1e-1
        		,2.0e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
        addComponent("CONTROL"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 1, 1)
        		//,5.0 + ExponentialDistributionGenerator.getNext(control_mips)*1e-1
        		,5.0e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
        addComponent("MAPS"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 2, 5)
        		//,10.0 + ExponentialDistributionGenerator.getNext(maps_mips)*1e-1
        		,10.0e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		,data_size*SimulationConstants.task_multiplier
        		);
        addComponent("PATH_CALC"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 2, 10)
        		//,15.0 + ExponentialDistributionGenerator.getNext(path_calc_mips)*1e-1
        		,15.0e3*SimulationConstants.task_multiplier
        		,data_size*SimulationConstants.task_multiplier
        		,data_size*SimulationConstants.task_multiplier
        		);
        addComponent("TRAFFIC"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 1, 1)
        		//,10.0 + ExponentialDistributionGenerator.getNext(traffic_mips)*1e-1
        		,10.0e3*SimulationConstants.task_multiplier
        		,data_size*SimulationConstants.task_multiplier
        		,20e3*SimulationConstants.task_multiplier
        		);
        addComponent("VOICE_SYNTH"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 1, 1)
        		,false
        		//,ExponentialDistributionGenerator.getNext(voice_synth_mips)*1e-1
        		,voice_synth_mips*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		,2e3*SimulationConstants.task_multiplier
        		);
        addComponent("GUI"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 0.5, 1)
        		,false
        		//,2.0 + ExponentialDistributionGenerator.getNext(gui_mips)*1e-1
        		,2.0e3*SimulationConstants.task_multiplier
        		,10e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		);
        addComponent("SPEED_TRAP"+"_"+getWorkloadId()+","+mobileId,
        		this.getMobileId(),
        		asList("python")
        		,new Hardware(1, 0.5, 1)
        		,false
        		//,2.0 + ExponentialDistributionGenerator.getNext(speed_mips)*1e-1
        		,2.0e3*SimulationConstants.task_multiplier
        		,10e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		);
	}
	
	
	@Override
	public void sample() {
		S.clear();
		L.clear();
		sampleTasks();
		sampleLinks();
	}

	@Override
	public MobileApplication clone() {
		NavigatorAppExp cloned = new NavigatorAppExp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = this.L;
		return cloned;
	}

}
