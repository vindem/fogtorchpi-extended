package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class AntivirusApp extends MobileApplication{

	private int small_files_n = SimulationConstants.antivirusSmallFiles;
	private int average_files_n = SimulationConstants.antivirusAverageFiles;
	private int big_files_n = SimulationConstants.antivirusBigFiles;
	
	public AntivirusApp(){
		super();
		sampleTasks();
		sampleLinks();
	}
	
	public AntivirusApp(int workload_id){
		super(workload_id);
		sampleTasks();
		sampleLinks();
	}
	
	public AntivirusApp(int workload_id,String mobileId){
		super(workload_id,mobileId);
		sampleTasks();
		sampleLinks();
	}
	
	
	@Override
	public MobileApplication clone() {
		AntivirusApp cloned = new AntivirusApp();
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = this.L;
		cloned.setSmall_files_n(small_files_n);
		cloned.setAverage_files_n(average_files_n);
		cloned.setBig_files_n(big_files_n);
		return cloned;
	}

	@Override
	public void sampleLinks() {
		addLink("ANTIVIRUS_UI"+"_"+getWorkloadId()+","+mobileId, "LOAD_DEFINITIONS"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("ANTIVIRUS_UI"+"_"+getWorkloadId()+","+mobileId, "SCAN_FILE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				5);
		addLink("LOAD_DEFINITIONS"+"_"+getWorkloadId()+","+mobileId,"COMPARE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("SCAN_FILE"+"_"+getWorkloadId()+","+mobileId,"COMPARE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("COMPARE"+"_"+getWorkloadId()+","+mobileId,"ANTIVIRUS_OUTPUT"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		/*for(int i = 0; i < small_files_n; i++)
		{
			addLink("ANTIVIRUS_UI", "SCAN_SMALL_FILE_"+i, 60, 5);
			addLink("LOAD_DEFINITIONS","SCAN_SMALL_FILE_"+i,30,0.1);
			addLink("SCAN_SMALL_FILE_"+i,"ANTIVIRUS_OUTPUT",30,0.1);
		}
		for(int i = 0; i < average_files_n; i++)
		{
			addLink("ANTIVIRUS_UI", "SCAN_AVERAGE_FILE_"+i, 60, 5);
			addLink("LOAD_DEFINITIONS","SCAN_AVERAGE_FILE_"+i,30,0.1);
			addLink("SCAN_AVERAGE_FILE_"+i,"ANTIVIRUS_OUTPUT",30,0.1);
		}
		for(int i = 0; i < big_files_n; i++)
		{
			addLink("ANTIVIRUS_UI", "SCAN_BIG_FILE_"+i, 60, 5);
			addLink("LOAD_DEFINITIONS","SCAN_BIG_FILE_"+i,30,0.1);
			addLink("SCAN_BIG_FILE_"+i,"ANTIVIRUS_OUTPUT",30,0.1);
		}*/
		
	}

	@Override
	public void sampleTasks() {
		/*
		small_files_n = (int) Math.ceil(ExponentialDistributionGenerator.getNext(75e-1)) + 1;
		average_files_n = (int) Math.ceil(ExponentialDistributionGenerator.getNext(25e-1)) + 1;
		big_files_n = (int) Math.ceil(ExponentialDistributionGenerator.getNext(5e-1)) + 1;
		*/
		//double file_size = ExponentialDistributionGenerator.getNext(SimulationConstants.file_size);
		double file_size = SimulationConstants.file_size;
		addComponent("ANTIVIRUS_UI"+"_"+getWorkloadId()+","+mobileId, this.getMobileId(), asList("python"),
				new Hardware(1, 1, 1)
				,false
				//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/4.0)+4.0)
        		,4.0e3*SimulationConstants.task_multiplier
				,5e+3*SimulationConstants.task_multiplier
        		,5e+3*SimulationConstants.task_multiplier
        		);
		addComponent("LOAD_DEFINITIONS"+"_"+getWorkloadId()+","+mobileId, this.getMobileId(),asList("python"),
				new Hardware(1, 1, 1)
				//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/2.0)+2.0)
				,2e3*SimulationConstants.task_multiplier
				,5e+3*SimulationConstants.task_multiplier
				,10e+3*SimulationConstants.task_multiplier
				);
		addComponent("SCAN_FILE"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId()
				,asList("python")
				,new Hardware(1,2,1)
				//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/2.0)+2.0)
				,2.0e3*SimulationConstants.task_multiplier
				,file_size*SimulationConstants.task_multiplier
				,5e+3*SimulationConstants.task_multiplier
				);
		addComponent("COMPARE"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python")
				,new Hardware(1,1,1)
				//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/2.0)+2.0)
				,2.0e3*SimulationConstants.task_multiplier
				,file_size + 1e3*SimulationConstants.task_multiplier
				,5e+3*SimulationConstants.task_multiplier
				);
		/*for(int i = 0; i < small_files_n; i++)
			addComponent("SCAN_SMALL_FILE_"+i,
					asList("python")
					,new Hardware((int) ExponentialDistributionGenerator.getNext(1.0/4.0)
							,ExponentialDistributionGenerator.getNext(1.0/2.0)
							,1)
					,ExponentialDistributionGenerator.getNext(1.0/2.0)
					,ExponentialDistributionGenerator.getNext(1.0/1e4)
					,5e+3);
		for(int i = 0; i < average_files_n; i++)
			addComponent("SCAN_AVERAGE_FILE_"+i,
				asList("python")
				,new Hardware((int) ExponentialDistributionGenerator.getNext(1.0/4.0)
						,ExponentialDistributionGenerator.getNext(1.0/2.0)
						,1)
				,ExponentialDistributionGenerator.getNext(1.0/2.0)
				,ExponentialDistributionGenerator.getNext(1.0/1e6)
				,5e+3);
		for(int i = 0; i < big_files_n; i++)
			addComponent("SCAN_BIG_FILE_"+i,
				asList("python")
				,new Hardware((int) ExponentialDistributionGenerator.getNext(1.0/4.0)
						,ExponentialDistributionGenerator.getNext(1.0/4.0)
						,1)
				,ExponentialDistributionGenerator.getNext(1.0/2.0)
				,ExponentialDistributionGenerator.getNext(1.0/1e9)
				,5e+3);*/
		addComponent("ANTIVIRUS_OUTPUT"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				//,ExponentialDistributionGenerator.getNext(1.0/2.0) + 2.0
				,2e3*SimulationConstants.task_multiplier
				,1e3*SimulationConstants.task_multiplier
				,5e3*SimulationConstants.task_multiplier
				);
						
	}

	public int getSmall_files_n() {
		return small_files_n;
	}

	public void setSmall_files_n(int small_files_n) {
		this.small_files_n = small_files_n;
	}

	public int getAverage_files_n() {
		return average_files_n;
	}

	public void setAverage_files_n(int average_files_n) {
		this.average_files_n = average_files_n;
	}

	public int getBig_files_n() {
		return big_files_n;
	}

	public void setBig_files_n(int big_files_n) {
		this.big_files_n = big_files_n;
	}

}
