package di.unipi.socc.fogtorchpi.deployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import java.util.Collections;
import java.util.List;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.MobileDevice;

public abstract class Search {

    protected MobileApplication A, tmpApp;
    protected MobileCloudInfrastructure I;
    protected HashMap<String, ArrayList<ComputationalNode>> K;
    protected HashMap<String, HashSet<String>> businessPolicies;
    protected int steps;
    public ArrayList<Deployment> D;

    public Search(MobileApplication A, MobileCloudInfrastructure I) { //, Coordinates deploymentLocation
        this.A = A;
        this.I = I;
        tmpApp = (MobileApplication) A.clone();
        //K = new HashMap<>();
        //D = new ArrayList<>();
        businessPolicies = new HashMap<String, HashSet<String>>();
        //keepLight = new ArrayList<>();
        //for (SoftwareComponent s: A.S){
        //     K.put(s.getId(), new ArrayList<>());
        //}
        
        //this.deploymentLocation = deploymentLocation;
    }

    public Search(MobileApplication A, MobileCloudInfrastructure I, HashMap<String, HashSet<String>> businessPolicies) {
            this.A = A;
            this.I = I;
            tmpApp = (MobileApplication) A.clone();
            K = new HashMap<>(); //(SOFTWARE, COMPATIBLENODES)
            D = new ArrayList<>(); 
            this.businessPolicies = businessPolicies;
            //keepLight = new ArrayList<>();
            for (SoftwareComponent s: A.S){
                 K.put(s.getId(), new ArrayList<>());
            }
    }

    protected boolean findCompatibleNodes() {
    	MobileDevice device = I.getMobileDevice();
    	for (SoftwareComponent s : A.S) {
    			if (device.isCompatible(s) 
                    && ((businessPolicies.containsKey(s.getId()) && businessPolicies.get(s.getId()).contains(device.getId()))
                    || !businessPolicies.containsKey(s.getId()))) 
        		{
        			//System.out.println("Inserisco " + device.getId() + " in K( " + s.getId() + " );");
        			K.get(s.getId()).add(device);
        		}
        
    	}
    	
    	
        for (SoftwareComponent s : A.S) {
        	MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
            if(!msc.isOffloadable())
            	continue;
        	for (CloudDatacentre n : I.C.values()) {
                if (n.isCompatible(s) && checkThings(s, n)
                        && ((businessPolicies.containsKey(s.getId()) 
                        && businessPolicies.get(s.getId()).contains(n.getId()))
                        || !businessPolicies.containsKey(s.getId()))) {
                  
                    K.get(s.getId()).add(n);
                }
            }
        }

        for (SoftwareComponent s : A.S) {
        	MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
            if(!msc.isOffloadable())
            	continue;
        	for (FogNode n : I.F.values()) {
                //System.out.println(s.getId() + " " + n.getId() + " "+ checkThings(s,n));
                if (n.isCompatible(s) && checkThings(s, n)
                        && ((businessPolicies.containsKey(s.getId()) && businessPolicies.get(s.getId()).contains(n.getId()))
                        || !businessPolicies.containsKey(s.getId()))) {
                    K.get(s.getId()).add(n);
                }
            }
        }
        boolean ret = true;
        for (SoftwareComponent s : A.S) {
            if (K.get(s.getId()).isEmpty()){
            	//System.out.println("Non riesco a piazzare " + s.getId());
            	ret = false;
            }
            bestFirst(K.get(s.getId()), s);
        }

        return ret;
    }

    public abstract ArrayList<Deployment> findDeployments(Deployment d); /*{
          
    
    /**
     * It prunes the search space till the first result. 
     * Uses the heuristics.
     * @param deployment
     * @return an eligible deployment.
     */
    public Deployment search(Deployment deployment) {
        if (isComplete(deployment)) {
        	D.add((Deployment) deployment.clone());
            return deployment;
        }
        SoftwareComponent s = selectUndeployedComponent(deployment);
        MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
        if(!msc.isOffloadable())
           	deploy(deployment,s,I.getMobileDevice());
        search(deployment);
        if (K.get(s.getId()) != null) {
            for (ComputationalNode n : K.get(s.getId())) { // for all nodes compatible with s
                //System.out.println(steps + " Checking " + s.getId() + " onto node " + n.getId());
                if (isValid(deployment, (MobileSoftwareComponent) s, n)) {
                    //System.out.println("Deploying " + s.getId() + " onto node " + n.getId());
                    deploy(deployment, s, n);
                    Deployment result = search(deployment);
                    if (result != null) {
                        return deployment;
                    }
                    else
                    undeploy(deployment, s, n);
                }
                //System.out.println("Undeploying " + s.getId() + " from node " + n.getId());
            }
        }
        return deployment;
    }
    /**
     * It returns all existing eligible deployment plans.
     * Results are sorted according to the heuristics (no pruning).
     * @param deployment 
     */
    private void exhaustiveSearch(Deployment deployment) {
        if (isComplete(deployment)) {
            D.add((Deployment) deployment.clone());
            //System.out.println(deployment);
            return;
        }
        SoftwareComponent s = selectUndeployedComponent(deployment);
        //System.out.println("Selected :" + s);
        ArrayList<ComputationalNode> Ks = bestFirst(K.get(s.getId()),s);
        //System.out.println(K.get(s.getId()));
        for (ComputationalNode n : Ks) { // for all nodes compatible with s 
            steps++;
            //System.out.println(steps + " Checking " + s.getId() + " onto " + n.getId());
            if (isValid(deployment, (MobileSoftwareComponent) s, n)) {
                //System.out.println(steps + " Deploying " + s.getId() + " onto " + n.getId());
                deploy(deployment, s, n);
                exhaustiveSearch(deployment);
                undeploy(deployment, s, n);
            }
            //System.out.println(steps + " Undeploying " + s.getId() + " from " + n.getId());
        }

    }
    
    protected ArrayList<ComputationalNode> bestFirst(ArrayList<ComputationalNode> Ks, SoftwareComponent s) {
        for (ComputationalNode n : Ks) {
            n.computeHeuristic(s);//, this.deploymentLocation);
        }
        Collections.sort(Ks);
        return Ks;
    }

    private boolean isOffloadPossibleOn(Deployment deployment, SoftwareComponent s, ComputationalNode n){
    	Couple<String,String> l1,l2;
    	l1 = new Couple<String,String>(I.getMobileDevice().getId(),n.getId());
    	l2 = new Couple<String,String>(n.getId(),I.getMobileDevice().getId());
    	if(I.L.containsKey(l1) && I.L.containsKey(l2))
    	{
    		QoSProfile q1 = I.L.get(l1);
    		QoSProfile q2 = I.L.get(l2);
    		return q1.getBandwidth() > 0 && q2.getBandwidth() > 0;
    	}
    	return false;
    }
    
    private boolean checkLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
    	for (SoftwareComponent c : deployment.keySet()) {
    		ComputationalNode m = deployment.get(c); // nodo deployment c
    		Couple couple1 = new Couple(c.getId(), s.getId());
    		Couple couple2 = new Couple(s.getId(), c.getId());
    		if (A.L.containsKey(couple1)) {
    			QoSProfile req1 = A.L.get(couple1);
    			QoSProfile req2 = A.L.get(couple2);
    			Couple c1 = new Couple(m.getId(), n.getId());
    			Couple c2 = new Couple(n.getId(), m.getId());
    			//System.out.println("Finding a link for " + couple1 + " between " + c1);
    			if (I.L.containsKey(c1)) {
    				QoSProfile off1 = I.L.get(c1);
    				QoSProfile off2 = I.L.get(c2);

    				if (req1 != null && !off1.supports(req1) || req2 != null && !off2.supports(req2)) 
    					return false;

    				if(I.getTransmissionTime((MobileSoftwareComponent) s, I.getMobileDevice().getId(),
    						deployment.get(c).getId()) > 
    				I.getTransmissionTimeWithQoS((MobileSoftwareComponent) s, I.getMobileDevice().getId(),
    						deployment.get(c).getId(), req1) )
    					return false;
    			} else {
    				//System.out.println("It does not exist");
    				return false;
    			}
    		}
    	}
    	return true;
    }

    protected void deployLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        for (SoftwareComponent c : deployment.keySet()) {
            ComputationalNode m = deployment.get(c);
            Couple couple1 = new Couple(c.getId(), s.getId());
            Couple couple2 = new Couple(s.getId(), c.getId());

            if (A.L.containsKey(couple1) && A.L.containsKey(couple2)) {
                QoSProfile req1 = A.L.get(couple1); //c,s
                QoSProfile req2 = A.L.get(couple2); //s,c
                Couple c1 = new Couple(m.getId(), n.getId()); // m,n
                Couple c2 = new Couple(n.getId(), m.getId()); // n,m
                if (I.L.containsKey(c1)) {
                    QoSProfile pl1 = I.L.get(c1);
                    QoSProfile pl2 = I.L.get(c2);
                    pl1.setBandwidth(pl1.getBandwidth() - req1.getBandwidth());
                    pl2.setBandwidth(pl2.getBandwidth() - req2.getBandwidth());
                }
            }
        }

        for (ThingRequirement t : s.Theta) {
            ExactThing e = (ExactThing) t;
            if (n.isReachable(e.getId(), I, e.getQNodeThing(), e.getQThingNode())) {
                Couple c1 = new Couple(n.getId(), e.getId()); //c1 nodeThing

                QoSProfile pl1 = I.L.get(c1);
                QoSProfile pl2 = I.L.get(new Couple(e.getId(), n.getId()));

                pl1.setBandwidth(pl1.getBandwidth() - e.getQNodeThing().getBandwidth());
                pl2.setBandwidth(pl2.getBandwidth() - e.getQThingNode().getBandwidth());

            }
        }
    }

    private boolean checkThings(SoftwareComponent s, ComputationalNode n) {
        //System.out.println("Checking things for "+ s.getId() + " -- " + n);
        for (ThingRequirement r : s.getThingsRequirements()) {
            //System.out.println(s.getId() + " " + r.toString());
            if (r.getClass() == ExactThing.class) {
                ExactThing e = (ExactThing) r;
                if (!n.isReachable(e.getId(), I, e.getQNodeThing(), e.getQThingNode())) { // directly or remotely
                    //System.out.println("Not reachable "+e.getId() +" from " + n.getId());
                    return false;
                }
            }
        }

        return true;
    }

    protected void undeployLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        for (SoftwareComponent c : deployment.keySet()) {
            ComputationalNode m = deployment.get(c);
            Couple couple1 = new Couple(c.getId(), s.getId());
            Couple couple2 = new Couple(s.getId(), c.getId());

            if (A.L.containsKey(couple1) && A.L.containsKey(couple2)) {
                QoSProfile al1 = A.L.get(couple1);
                QoSProfile al2 = A.L.get(couple2);
                Couple c1 = new Couple(m.getId(), n.getId());
                Couple c2 = new Couple(n.getId(), m.getId());
                if (I.L.containsKey(c1)) {
                    QoSProfile pl1 = I.L.get(c1);
                    QoSProfile pl2 = I.L.get(c2);

                    pl1.setBandwidth(pl1.getBandwidth() + al1.getBandwidth());
                    pl2.setBandwidth(pl2.getBandwidth() + al2.getBandwidth());
                }
            }

        }

        for (ThingRequirement t : s.Theta) {
            ExactThing e = (ExactThing) t;
            //System.out.println("Request" + e);
            if (n.isReachable(e.getId(), I, e.getQNodeThing(), e.getQThingNode())) {
                Couple c1 = new Couple(n.getId(), e.getId());

                QoSProfile pl1 = I.L.get(c1);
                QoSProfile pl2 = I.L.get(new Couple(e.getId(), n.getId()));

                pl1.setBandwidth(pl1.getBandwidth() + e.getQNodeThing().getBandwidth());
                pl2.setBandwidth(pl2.getBandwidth() + e.getQThingNode().getBandwidth());

            }
        }
    }

    protected boolean isValid(Deployment deployment, MobileSoftwareComponent s, ComputationalNode n) {
        //System.out.println(n.getId() + " is Compatible " + n.isCompatible(s)  );
        //System.out.println(n.getId() + " links " + checkLinks(deployment, s, n)  );
        //System.out.println(n.getId() + " things " + checkThings(s, n));
        /*return n.isCompatible(s) && checkLinks(deployment, s, n) 
        		&& checkThings(s, n) && isOffloadPossibleOn(deployment, s, n);*/
    	double consOnMobile = (n.isMobile())? n.getCPUEnergyModel().computeCPUEnergy(s, n, I) : I.getMobileDevice().getNetEnergyModel().computeNETEnergy(s, n, I) ;
        return n.isCompatible(s,I) && isOffloadPossibleOn(deployment, s, n)
        		&& deployment.mobileEnergyBudget - consOnMobile >= 0 && checkLinks(deployment,s,n);
    }

    protected synchronized void deploy(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        SimulationConstants.logger.info("Adding " + s.getId() + " = " + deployment.containsKey(s));
    	deployment.put(s, n);
        deployment.addCost(s,n, I);
        deployment.addEnergyConsumption(s, n, I);
        //deployment.addRuntime(s, n, I);
        //System.out.println(deployment + " " + deployment.size());
        n.deploy(s);
        deployLinks(deployment, s, n);
    }

    protected void undeploy(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        if (deployment.containsKey(s)) {
        	n.undeploy(s);
        	deployment.removeRuntime(s, n, I);
        	deployment.removeCost(s, n, I);
            deployment.removeEnergyConsumption(s, n, I);
        	deployment.remove(s);
            undeployLinks(deployment, s, n);
            
        }
       // System.out.println("UNDEP"+deployment);
    }

    protected SoftwareComponent selectUndeployedComponent(Deployment deployment) {
        //System.out.println(deployment.size());
        return A.S.get(deployment.size());
    }
    
    //Used for DAG SCHEDULING
    protected ArrayList<MobileSoftwareComponent> selectElegibleNodes(){
    	ArrayList<MobileSoftwareComponent> elegibleNodes = new ArrayList<MobileSoftwareComponent>();
    	for(int i = 0; i < tmpApp.S.size(); i++){
    		if(tmpApp.incidentEdgesIn(tmpApp.S.get(i)).size() == 0)
    			elegibleNodes.add((MobileSoftwareComponent) tmpApp.S.remove(i));    		    		
    	}
    	return elegibleNodes;    	
    }
    
    protected boolean isComplete(Deployment deployment) {
        if(deployment == null)
        	return false;
    	for(SoftwareComponent s : A.S)
        	if(!deployment.containsKey(s))
        		return false;
        return true;
    }

    public void addBusinessPolicies(String component, List<String> allowedNodes) {
        HashSet<String> policies = new HashSet<>();
        policies.addAll(allowedNodes);
        this.businessPolicies.put(component, policies);
    }
    
    public void addKeepLightNodes(List<String> keepLightNodes) {
        for (String n : keepLightNodes){
            if (I.C.containsKey(n)){
                I.C.get(n).setKeepLight(true);
            }
            if(I.F.containsKey(n)){
                I.F.get(n).setKeepLight(true);
            }
                
        }
    }
    
    public void resetSearch(Deployment deployment) {
    	if(deployment!=null)
    	{
    		Object[] softwareList = deployment.keySet().toArray();
    		for(Object s: softwareList)
    		{
    			SoftwareComponent a = (SoftwareComponent) s;
    			ComputationalNode n = deployment.get(a);
    			undeploy(deployment,a,n);
    		}
    	}
	}
    
    public void executeDeployment(Deployment d){

        Deployment dep = new Deployment();
        dep.deploymentMonthlyCost = d.deploymentMonthlyCost;
        
        for (SoftwareComponent component : d.keySet()){
            ComputationalNode n = d.get(component);
            dep.put(component, n);
            n.deploy(component);
            deployLinks(dep, component, n);
        }

    }

    

}
