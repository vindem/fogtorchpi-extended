package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class MOHeftResearch extends DAGResearch {

	class RankComparator implements Comparator<MobileSoftwareComponent>
	{

		private MobileApplication A;
		private MobileCloudInfrastructure I;
		
		public RankComparator(MobileApplication A, MobileCloudInfrastructure I)
		{
			this.A = A;
			this.I = I;
		}
		@Override
		public int compare(MobileSoftwareComponent o1, MobileSoftwareComponent o2) {
			if(o1.getNodeRank() > o2.getNodeRank())
				return -1;
			else if(o1.getNodeRank() < o2.getNodeRank())
				return 1;
			else
				return 0;
		}
			
	}
	
	public MOHeftResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}

	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(A.S.size(), new RankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		
		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0.0;
		
		while(!isComplete(deployment)){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
			}
			double tMin = Double.MAX_VALUE;
			ComputationalNode target = null;
			MobileSoftwareComponent currTask = tasks.poll();
			if(!currTask.isOffloadable() 
					&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
			{
				target = I.getMobileDevice(currTask.getUid());
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else{
				double maxP = Double.MIN_VALUE;
				for(MobileSoftwareComponent cmp : A.getPredecessors(currTask))
					if(cmp.getRuntime() > maxP)
						maxP = cmp.getRuntime();
				for(CloudDatacentre cdc : I.C.values())
					if(maxP + currTask.getRuntimeOnNode(cdc, I) < tMin
							&& isValid(deployment, currTask, cdc))
					{
						tMin = maxP + currTask.getRuntimeOnNode(cdc, I);
						target = cdc;
					}
				for(FogNode fn : I.F.values())
					if(maxP + currTask.getRuntimeOnNode(fn, I) < tMin
							&& isValid(deployment, currTask, fn))
					{
						tMin = maxP + currTask.getRuntimeOnNode(fn, I);
						target = fn;
					}
				if(maxP + currTask.getRuntimeOnNode(I.getMobileDevice(currTask.getUid()), I) < tMin
						&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
					target = I.getMobileDevice(currTask.getUid());
			}
			if(target!=null)
			{
				deploy(deployment,currTask,target);
				setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else
			{
				if(scheduledNodes.isEmpty())
					return null;
			}
		}
		scheduledNodes = null;
		deployment.runTime += currentRuntime;
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		deployments.add(deployment);
		return deployments;
	}
	
	
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
