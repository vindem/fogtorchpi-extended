package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import at.ac.tuwien.ec.algorithms.utils.NodeRankComparator;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class HeftCostResearch extends DAGResearch {

	
	
	public HeftCostResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(A.S.size(), new NodeRankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();


		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0.0;

		while(!isComplete(deployment)){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
			}
			double tMin = Double.MAX_VALUE;
			ComputationalNode target = null;
			MobileSoftwareComponent currTask = tasks.poll();
			if(!currTask.isOffloadable() 
					&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
			{
				target = I.getMobileDevice(currTask.getUid());
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else{
				double minCost = Double.MAX_VALUE;
				double batteryThreshold = SimulationConstants.batteryBudget * 0.2;
				if(deployment.mobileEnergyBudget < batteryThreshold)
				{			
					for(CloudDatacentre cdc : I.C.values())
						if(cdc.computeCost(currTask, I).getCost() < minCost
								&& isValid(deployment, currTask, cdc))
						{
							minCost = cdc.computeCost(currTask, I).getCost();
							target = cdc;
						}
					for(FogNode fn : I.F.values())
						if(fn.computeCost(currTask, I).getCost() < minCost
								&& isValid(deployment, currTask, fn))
						{
							minCost = fn.computeCost(currTask, I).getCost();
							target = fn;
						}
				}
				else if(isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
				{
					minCost = 0.0;
					target = I.getMobileDevice(currTask.getUid());
				}
			}
			if(target!=null)
			{
				deploy(deployment,currTask,target);
				setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else
			{
				if(scheduledNodes.isEmpty())
					return null;
			}
		}
		while(!scheduledNodes.isEmpty())
		{
			MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
			currentRuntime = firstTaskToTerminate.getRuntime();
			deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
			scheduledNodes.remove(firstTaskToTerminate);
		}
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		deployments.add(deployment);
		return deployments;
	}
	
	
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
