package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class NavigatorApp extends MobileApplication {

	public double data_variance = 1e+3;
	
	double config_panel_mips = 0.5, cf_mips_var = 0.15;
    double gps_mips = 0.5, gps_mips_var = 0.15;
    double control_mips = 1.3, control_mips_var = 0.2;
    double maps_mips = 3, maps_mips_var = 0.6;
    double path_calc_mips = 5, path_calc_mips_var = 1.0;
    double traffic_mips = 3, traffic_mips_var = 0.6;
    double voice_synth_mips = 3, voice_synth_mips_var = 0.6;
    double gui_mips = 0.5, gui_mips_var = 0.15;
    double speed_mips = 0.5, speed_mips_var = 0.15;
    
    double maps_in_data = 5e+6, maps_in_data_var = data_variance;
    double maps_out_data = 25e+6, maps_out_data_var = data_variance;
    double path_calc_in_data = 25e+6, path_calc_in_data_var = data_variance;
    double path_calc_out_data = 10e+6, path_calc_out_data_var = data_variance;
    double traffic_in_data = 10e3, traffic_in_data_var = data_variance;
    double traffic_out_data = 2e3, traffic_out_data_var = data_variance;
	
	public NavigatorApp(){
        sampleTasks();
        sampleLinks();
	}

	public void sampleLinks() {
		addLink("CONF_PANEL", "GPS", 60, 0.1);
        addLink("CONF_PANEL", "CONTROL", 60, 0.1);
        addLink("GPS", "CONTROL", 30, 0.1);
        addLink("CONTROL", "MAPS", 30, 0.1);
        addLink("CONTROL", "PATH_CALC", 30, 0.1, 0.1);
        addLink("CONTROL", "TRAFFIC", 60, 0.1, 0.1);
        addLink("MAPS", "PATH_CALC", 30, 0.1, 0.1);
        addLink("TRAFFIC", "PATH_CALC", 60, 0.1, 0.1);
        addLink("PATH_CALC", "VOICE_SYNTH", 60, 0.1, 0.1);
        addLink("PATH_CALC", "GUI", 60, 0.1, 0.1);
        addLink("PATH_CALC", "SPEED_TRAP", 30, 0.1, 0.1);
	}

	public void sampleTasks() {
		addComponent("CONF_PANEL",
				this.getMobileId(),
				asList("python"), new Hardware(1, 1, 1),false,SimulationConstants.rand.nextGaussian() * cf_mips_var + config_panel_mips
        		,5e+3
        		,5e+3);
        addComponent("GPS",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 3, 1),false,SimulationConstants.rand.nextGaussian() * gps_mips_var + gps_mips
        		,5e+3
        		,5e+3);
        addComponent("CONTROL",
        		this.getMobileId(),
        		asList("python"), new Hardware(2, 3, 1),SimulationConstants.rand.nextGaussian() * control_mips_var + control_mips
        		,5e+3
        		,5e+3);
        addComponent("MAPS",
        		this.getMobileId(),
        		asList("python"),
        		new Hardware(2, 5, 5),SimulationConstants.rand.nextGaussian() * maps_mips_var + maps_mips
        		,SimulationConstants.rand.nextGaussian() * maps_in_data_var + maps_in_data
        		,SimulationConstants.rand.nextGaussian() * maps_out_data_var + maps_out_data);
        addComponent("PATH_CALC",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 2, 10),SimulationConstants.rand.nextGaussian() * path_calc_mips_var + path_calc_mips
        		,SimulationConstants.rand.nextGaussian() * path_calc_in_data_var + path_calc_in_data
        		,SimulationConstants.rand.nextGaussian() * path_calc_out_data_var + path_calc_out_data);
        addComponent("TRAFFIC",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 1, 1),SimulationConstants.rand.nextGaussian() * traffic_mips_var + traffic_mips
        		,SimulationConstants.rand.nextGaussian() * traffic_in_data_var + traffic_in_data
        		,SimulationConstants.rand.nextGaussian() * traffic_out_data_var + traffic_out_data);
        addComponent("VOICE_SYNTH",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 1, 1),false
        		,SimulationConstants.rand.nextGaussian() * voice_synth_mips_var + voice_synth_mips
        		,1e+3
        		,2e+3);
        addComponent("GUI",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 1, 1),false
        		,SimulationConstants.rand.nextGaussian() * gui_mips_var + gui_mips
        		,10e+3
        		,1e+3);
        addComponent("SPEED_TRAP",
        		this.getMobileId(),
        		asList("python"), new Hardware(1, 1, 1),false
        		,SimulationConstants.rand.nextGaussian() * speed_mips_var + speed_mips
        		,10e+3
        		,1e+3);
	}
	
	
	@Override
	public void sample() {
		S.clear();
		L.clear();
		sampleTasks();
		sampleLinks();
	}

	@Override
	public MobileApplication clone() {
		NavigatorApp cloned = new NavigatorApp();
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = this.L;
		return cloned;
	}

}
