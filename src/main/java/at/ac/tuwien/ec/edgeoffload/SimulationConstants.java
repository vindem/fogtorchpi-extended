package at.ac.tuwien.ec.edgeoffload;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SimulationConstants {

	/*
	 * SIMULATION RELATED CONSTANTS	
	 */
	public static String outfile = "./results/";
	public static boolean batch = false;
	public static int offloadable_part_repetitions = 1;
	public static int[] appExecutions = {5};
	//public static int[] appExecutions = {100};
	public static double Eta = 1.0;
	public static String targetApp = "MULTI";
	public static int simulation_runs = 10;
	//public static String chosenAlgorithm = "weighted";
	public static Level logLevel = Level.OFF;
	public static double workload_runs_lambda = 1.0;
	//PROBABILITY DISTRIBUTIONS
	public static final Random rand = new Random();
	//DEBUGGING
	public static final Logger logger = Logger.getLogger("FogTorchPILogger");
	/* 
	 * APP RELATED CONSTANTS
	 */
	//QoS requirements
	
	//ANTIVIRUS
	public static final int antivirusSmallFiles = 1;
	public static final int antivirusAverageFiles = 1;
	public static final int antivirusBigFiles = 1;
	//lambda
	public static double file_size = 10e3;
	//FACERECOGNIZER
	public static double image_size = 10e4; //lambda
	//NAVIGATOR
	public static double maps_size = 20e3; //lambda
	//CHESS
	public static final int chessMoves = 1;
	public static double chess_mi = 8e3; //lambda
	
	/*
	 *  INFRASTRUCTURE RELATED CONSTANTS
	 */
	public static boolean cloudOnly = false;
	public static double alpha = 1.0, beta = 0, gamma = 0;
	public static double core_variance = 2.6;
	public static int cloudNodes = 6;
	public static int edgeNodes = 4;
	public static int mobileNum = 36;
	public static final int cloudNodesOnlyCloud = cloudNodes + edgeNodes;
	public static final double confidenceLevel = 2.576;
	//public static String[] algorithms = {"minmin","mincost", "maxbatt"};
	public static String[] algorithms = {"heft"};
	//public static String[] algorithms;
	public static final double battery_watt_hour = 7.4;
	public static final double seconds_per_hour = 3600;
	public static final double task_multiplier = 10e2;
	public static final double batteryCapacity = (battery_watt_hour * seconds_per_hour) * mobileNum *30.0;
	public static int MAP_M = 23;
	public static int MAP_N = 12;
	public static int edgeCoreNum = 128;
	public static double batteryBudget = batteryCapacity;
	public static int cores = Runtime.getRuntime().availableProcessors();
	public static String traceFile = "C:/FogTorchPI-costmodel/"
    		+ "FogTorchPI-costmodel/traces/prices-20180927realtime_zone.csv";
	public static String edgePlanningAlgorithm = "random";
	public static double lambdaLatency = 0;
	public enum Timezone
	{
		INDIANAPOLIS(39.7794476,-86.4129515,-6),
		DETROIT(42.3526257,-83.2392927,-6),
		DUBLIN(53.3239919,-6.5258936,0),
		STGHISLAIN(50.489824,3.7378795,0),
		SINGAPORE(1.3139961,103.7041579,7),
		KOREA(37.5650172,126.8494614,8);
		
		private final double xCoord,yCoord;
		private final int timeOffset;
		
		Timezone(double x,double y, int offset){
			xCoord = x;
			yCoord = y;
			timeOffset = offset;
		}
		
		public double getX()
		{
			return xCoord;
		}
		
		public double getY()
		{
			return yCoord;
		}
		
		public int getTZOffset(){
			return timeOffset;
		}
	}
}
