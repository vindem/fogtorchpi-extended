/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.ac.tuwien.ec.appmodel;

import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import at.ac.tuwien.ec.appmodel.mobileapps.NavigatorApp;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;

import static java.util.Arrays.asList;

/**
 *
 * @author Vincenzo De Maio
 */
public abstract class MobileApplication extends Application{
    
	protected int workload_id;
	protected String mobileId;
	
	public MobileApplication(){
		super();
		workload_id = 0;
	}
	
	public MobileApplication(int wId){
		super();
		workload_id = wId;
	}
	
	public MobileApplication(int wId,String mobileId){
		super();
		workload_id = wId;
		this.mobileId = mobileId;
	}
	
	public int getWorkloadId(){
		return workload_id;
	}
	
	public void setWorkloadId(int id){
		this.workload_id = id;
	}
	
	public void setMobileId(String uid)
	{
		this.mobileId = uid;
	}
	
	public String getMobileId()
	{
		return mobileId;
	}
	
	public void addComponent(String id, String uid, List<String> softwareReqs, Hardware hardwareReqs) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id, uid, softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(true);
		cmp.setUid(mobileId);
		S.add(cmp);
    }
	
	public void addComponent(String id, String uid, List<String> softwareReqs, Hardware hardwareReqs,boolean offloadable) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id,uid,softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(offloadable);
		cmp.setUid(mobileId);
		S.add(cmp);   
    }
	
	
	public void addComponent(String id, String uid, List<String> softwareReqs, Hardware hardwareReqs,double mips) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id, uid,softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(true);
		cmp.setMips(mips);
		cmp.setInData(0.0);
		cmp.setOutData(0.0);
		S.add(cmp);   
    }

	public void addComponent(String id, String uid, List<String> softwareReqs, Hardware hardwareReqs,boolean offloadable, double mipsRequired) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id,uid,softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(offloadable);
		cmp.setMips(mipsRequired);
		cmp.setInData(0.0);
		cmp.setOutData(0.0);
		S.add(cmp);   
    }
	
	public void addComponent(String id, String uid, List<String> softwareReqs, Hardware hardwareReqs,double mips, double inData, double outData) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id,uid,softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(true);
		cmp.setMips(mips);
		cmp.setInData(inData);
		cmp.setOutData(outData);
		S.add(cmp);   
    }

	public void addComponent(String id, String uid,List<String> softwareReqs, Hardware hardwareReqs,boolean offloadable, double mipsRequired, double inData, double outData) {
		MobileSoftwareComponent cmp = new MobileSoftwareComponent(id, uid, softwareReqs, hardwareReqs, new ArrayList<>());
		cmp.setOffloadable(offloadable);
		cmp.setMips(mipsRequired);
		if(inData < 0)
			inData = 0;
		cmp.setInData(inData);
		if(outData < 0)
			outData = 0;
		cmp.setOutData(outData);
		S.add(cmp);
	}
	
	public ArrayList<MobileSoftwareComponent> getPredecessors(SoftwareComponent msc) {
		ArrayList<Couple<String,String>> incidentEdges;
		ArrayList<MobileSoftwareComponent> preds = new ArrayList<MobileSoftwareComponent>();
		
		incidentEdges = incidentEdgesIn(msc);
		
		for(Couple<String,String> edge : incidentEdges)
			preds.add((MobileSoftwareComponent) getSoftwareById(edge.getA()));
		
		return preds;
	}

	public ArrayList<MobileSoftwareComponent> getNeighbors(SoftwareComponent msc){
		ArrayList<Couple<String,String>> outgoingEdges;
		ArrayList<MobileSoftwareComponent> neighs = new ArrayList<MobileSoftwareComponent>();
		
		outgoingEdges = outgoingEdgesFrom(msc);
		
		for(Couple<String,String> edge : outgoingEdges)
			neighs.add((MobileSoftwareComponent) getSoftwareById(edge.getB()));
		
		return neighs;
	}
	
	public SoftwareComponent getSoftwareById(String a) {
		for(SoftwareComponent msc : S){
			if(msc.getId().equals(a))
				return msc;
		}
		return null;
	}

	public double sampleLatency(){
		return ((SimulationConstants.lambdaLatency == 0)?
				Double.MAX_VALUE : 
					SimulationConstants.lambdaLatency + ExponentialDistributionGenerator
				.getNext(SimulationConstants.lambdaLatency)) ;
	}
		
	public void sample(){
		S.clear();
		L.clear();
		sampleTasks();
		sampleLinks();
	}

	public abstract MobileApplication clone();
	
	public abstract void sampleLinks();

	public abstract void sampleTasks();

	
}
