package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import at.ac.tuwien.ec.algorithms.utils.NodeRankComparator;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class HeftEchoResearch extends DAGResearch {

	private static final double gamma = SimulationConstants.gamma;
	private static final double alpha = SimulationConstants.alpha;
	private static final double beta = SimulationConstants.beta;
	
	public HeftEchoResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}

	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(A.S.size(), new NodeRankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		
		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0.0;
		
		while(!isComplete(deployment)){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
			}
			double minRuntime = Double.MAX_VALUE;
			double minCost = Double.MAX_VALUE;
			double maxBattery = Double.MIN_VALUE;
			
			ComputationalNode target = null;
			MobileSoftwareComponent currTask = tasks.poll();
			if(!currTask.isOffloadable() && isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
				{
					target = I.getMobileDevice(currTask.getUid());
					scheduledNodes.add(currTask);
					//tasks.remove();
				}
			else{
				for(CloudDatacentre cdc : I.C.values())
					if(isValid(deployment,currTask,cdc))
					{
						double tmpRuntime = currTask.getRuntimeOnNode(cdc, I);
						double tmpCost = cdc.computeCost(currTask, I).getCost();
						double tmpBattery = I.getMobileDevice(currTask.getUid()).getEnergyBudget() - 
								 I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, cdc, I);

						if(tmpRuntime < minRuntime)
							minRuntime = tmpRuntime;
						if(tmpCost < minCost)
							minCost = tmpCost;
						if(tmpBattery > maxBattery)
							maxBattery = tmpBattery;
					}
				
				for(FogNode fn : I.F.values())
					if(isValid(deployment,currTask,fn))
					{
						double tmpRuntime = currTask.getRuntimeOnNode(fn, I);
						double tmpCost = fn.computeCost(currTask, I).getCost();
						double tmpBattery = I.getMobileDevice(currTask.getUid()).getEnergyBudget() - 
								 I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, fn, I);

						if(tmpRuntime < minRuntime)
							minRuntime = tmpRuntime;
						if(tmpCost < minCost)
							minCost = tmpCost;
						if(tmpBattery > maxBattery)
							maxBattery = tmpBattery;
					}
				
				if(isValid(deployment,currTask,I.getMobileDevice(currTask.getUid())))
				{
					double tmpRuntime = currTask.getRuntimeOnNode(I.getMobileDevice(currTask.getUid()), I);
					double tmpCost = I.getMobileDevice(currTask.getUid()).computeCost(currTask, I).getCost();
					double tmpBattery = I.getMobileDevice(currTask.getUid()).getEnergyBudget() - I.getMobileDevice(currTask.getUid()).getCPUEnergyModel().computeCPUEnergy(currTask, I.getMobileDevice(currTask.getUid()), I); 
									

					if(tmpRuntime < minRuntime)
						minRuntime = tmpRuntime;
					if(tmpCost < minCost)
						minCost = tmpCost;
					if(tmpBattery > maxBattery)
						maxBattery = tmpBattery;
				}
				
				double minScore = Double.MAX_VALUE;
				double tmpScore;
				
				for(CloudDatacentre cdc : I.C.values())
				{
					if((tmpScore = computeScore(currTask,cdc,I,minRuntime,minCost,maxBattery)) < minScore
							&& isValid(deployment, currTask, cdc))
					{
						minScore = tmpScore;
						target = cdc;
					}
				}
				for(FogNode fn : I.F.values())
					if((tmpScore = computeScore(currTask,fn,I,minRuntime,minCost,maxBattery)) < minScore
							&& isValid(deployment, currTask, fn))
					{
						minScore = tmpScore;
						target = fn;
					}
				if((tmpScore = computeScore(currTask,I.getMobileDevice(currTask.getUid()),I,minRuntime,minCost,maxBattery)) < minScore
						&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
				{
					target = I.getMobileDevice(currTask.getUid());
				}
			}
			if(target!=null)
			{
				deploy(deployment,currTask,target);
				setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else
			{
				if(scheduledNodes.isEmpty())
					return null;
			}
		}
		while(!scheduledNodes.isEmpty())
		{
			MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
			currentRuntime = firstTaskToTerminate.getRuntime();
			deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
			scheduledNodes.remove(firstTaskToTerminate);
		}
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		deployments.add(deployment);
		return deployments;
	}

	private void findLocalOptimum(Deployment deployment, double minRuntime, double minCost, double maxBattery,
			MobileSoftwareComponent currTask, ComputationalNode cdc) {
		if(isValid(deployment,currTask,cdc))
		{
			double tmpRuntime = currTask.getRuntimeOnNode(cdc, I);
			double tmpCost = cdc.computeCost(currTask, I).getCost();
			double tmpBattery = I.getMobileDevice(currTask.getUid()).getEnergyBudget() - 
					((cdc.isMobile())? cdc.getCPUEnergyModel().computeCPUEnergy(currTask, cdc, I) 
							: I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, cdc, I));

			if(tmpRuntime < minRuntime)
				minRuntime = tmpRuntime;
			if(tmpCost < minCost)
				minCost = tmpCost;
			if(tmpBattery > maxBattery)
				maxBattery = tmpBattery;
		}
	}
	
	
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	private double computeScore(SoftwareComponent s, ComputationalNode cn, MobileCloudInfrastructure i, double minRuntime, double minCost, double maxBattery) {
		//if(cn.isMobile())
		//	return Double.MAX_VALUE;
		double currRuntime = s.getRuntimeOnNode(cn, I);
		double currCost = cn.computeCost(s, I).getCost();
		double currBattery = I.getMobileDevice(s.getUid()).getEnergyBudget() - 
				((cn.isMobile())? cn.getCPUEnergyModel().computeCPUEnergy(s, cn, I) 
				: I.getMobileDevice(s.getUid()).getNetEnergyModel().computeNETEnergy(s, cn, I));
		
		double runtimeDiff = Math.pow(currRuntime - minRuntime,2.0);
		double costDiff = Math.pow(currCost - minCost,2.0);
		double batteryDiff = Math.pow(maxBattery - currBattery,2.0);
				
		return alpha * adjust(runtimeDiff,1.0)  + beta * adjust(costDiff,1.0) + gamma * adjust(batteryDiff,1.0);
	}

	private double normalized(double x, double minRange, double maxRange) {
		return (x - minRange) / (maxRange - minRange) ;
	}
	
	private double adjust(double x, double factor){
		return x * factor;
	}
	
}
