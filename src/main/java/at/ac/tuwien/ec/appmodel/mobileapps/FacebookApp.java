package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class FacebookApp extends MobileApplication {

	public FacebookApp(){
		super();
		sampleTasks();
		sampleLinks();
	}
	
	public FacebookApp(int workload_id) {
		super(workload_id);
		sampleTasks();
		sampleLinks();
	}
	
	public FacebookApp(int workload_id, String mId) {
		super(workload_id, mId);
		sampleTasks();
		sampleLinks();
	}

	@Override
	public MobileApplication clone() {
		FacebookApp cloned = new FacebookApp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = (LinkedHashMap<Couple<String, String>, QoSProfile>) this.L.clone();
		return cloned;
	}

	@Override
	public void sampleLinks() {
		addLink("FACEBOOK_GUI"+"_"+getWorkloadId()+","+mobileId
				, "GET_TOKEN"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("FACEBOOK_GUI"+"_"+getWorkloadId()+","+mobileId,
				"POST_REQUEST"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("GET_TOKEN"+"_"+getWorkloadId()+","+mobileId
				, "POST_REQUEST"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("POST_REQUEST"+"_"+getWorkloadId()+","+mobileId
				, "PROCESS_RESPONSE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		//addLink("POST_REQUEST"+"_"+getWorkloadId(), "FILE_UPLOAD"+"_"+getWorkloadId(), Integer.MAX_VALUE, Double.MIN_VALUE);
		addLink("PROCESS_RESPONSE"+"_"+getWorkloadId()+","+mobileId
				, "FILE_UPLOAD"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("FILE_UPLOAD"+"_"+getWorkloadId()+","+mobileId
				, "APPLY_FILTER"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("APPLY_FILTER"+"_"+getWorkloadId()+","+mobileId
				, "FACEBOOK_POST"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
	}

	@Override
	public void sampleTasks() {
		//double img_size = ExponentialDistributionGenerator.getNext(SimulationConstants.image_size);
		double img_size = SimulationConstants.image_size;
		addComponent("FACEBOOK_GUI"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				,2.0e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		);
		addComponent("GET_TOKEN"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,3.0e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		);
		addComponent("POST_REQUEST"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,2.0e3*SimulationConstants.task_multiplier 
        		,1e3//*SimulationConstants.task_multiplier
        		,5e3//*SimulationConstants.task_multiplier
        		);
		addComponent("PROCESS_RESPONSE"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,2.0e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		,1e3*SimulationConstants.task_multiplier
        		);
		addComponent("FILE_UPLOAD"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				,5.0e3 *SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		);
		addComponent("APPLY_FILTER"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,8.0e3 *SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier);
		addComponent("FACEBOOK_POST"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				,2.0e3 *SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
	}

}
