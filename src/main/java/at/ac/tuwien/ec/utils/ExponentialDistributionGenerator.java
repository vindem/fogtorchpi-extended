package at.ac.tuwien.ec.utils;

import java.util.Random;

import at.ac.tuwien.ec.edgeoffload.SimulationConstants;

public class ExponentialDistributionGenerator {

	public static double getNext(double lambda){
		double val =  Math.log(1-SimulationConstants.rand.nextDouble())/(-lambda);
		while(val <= 0.0)
			val = Math.log(1-SimulationConstants.rand.nextDouble())/(-lambda);
		return Math.floor(val);
	}
	
}
