package at.ac.tuwien.ec.algorithms.mo;

import java.util.Random;

import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.util.pseudorandom.RandomGenerator;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class DeploymentMutationOperator implements MutationOperator<DeploymentSolution>{

	private Random mutationRandomGenerator = SimulationConstants.rand;
	private double mutationProbability;
	
	public DeploymentMutationOperator(double mutationProbability){
		this.mutationProbability = mutationProbability;
	}
	
	@Override
	public DeploymentSolution execute(DeploymentSolution arg0) {
		Deployment d = arg0.getDeployment();
		MobileSoftwareComponent n1,n2;
		int idx1,idx2;
		boolean ex = false;
		if (mutationRandomGenerator.nextDouble() < mutationProbability) {
			do
			{
				idx1 = SimulationConstants.rand.nextInt(d.size());
				while((idx2 = SimulationConstants.rand.nextInt(d.size())) == idx1) 
					;

				n1 = (MobileSoftwareComponent) d.keySet().toArray()[idx1];
				n2 = (MobileSoftwareComponent) d.keySet().toArray()[idx2];
				ex = n1.isOffloadable() && n2.isOffloadable();
			}
			while(!n1.isOffloadable() && !n2.isOffloadable() && !ex);

			ComputationalNode cn1 = d.get(n1);
			ComputationalNode cn2 = d.get(n2);
			
			arg0.setVariableValue(idx1, cn2);
			arg0.setVariableValue(idx2, cn1);
		}
		return new DeploymentSolution(d,arg0.getApplication(),arg0.getInfrastructure());
	}

}
