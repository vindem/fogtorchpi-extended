package at.ac.tuwien.ec.algorithms.mo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.logging.Level;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.algorithm.multiobjective.nsgaiii.NSGAIIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.util.AbstractAlgorithmRunner;
import org.uma.jmetal.runner.multiobjective.NSGAIIRunner;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.ProblemUtils;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.comparator.RankingComparator;

import at.ac.tuwien.ec.algorithms.DAGResearch;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class NSGAIIIResearch extends DAGResearch{

	DeploymentProblem problem;
    Algorithm<List<DeploymentSolution>> algorithm;
    CrossoverOperator<DeploymentSolution> crossover;
    MutationOperator<DeploymentSolution> mutation;
    SelectionOperator<List<DeploymentSolution>, DeploymentSolution> selection;
	private double crossoverProbability;
	private double mutationProbability;


	public NSGAIIIResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
		A.computeNodeRanks(I);
		this.problem = new DeploymentProblem(I, A);
		this.crossoverProbability = 0.9;
		this.mutationProbability = 1.0 / problem.getNumberOfVariables() ;
		crossover = new DeploymentCrossoverOperator(crossoverProbability);
		mutation = new DeploymentMutationOperator(mutationProbability);
		selection = new BinaryTournamentSelection<DeploymentSolution>(new RankingComparator<DeploymentSolution>());
	}

	
	@Override
	public ArrayList<Deployment> findDeployments(Deployment d) {
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		List<DeploymentSolution> population = new ArrayList<DeploymentSolution>();
		try{
			/*algorithm = new NSGAIIIBuilder<DeploymentSolution>(problem)
	            .setCrossoverOperator(crossover)
	            .setMutationOperator(mutation)
	            .setSelectionOperator(selection)
	            .setMaxIterations(100)
	            //.setPopulationSize(10)
	            .build() ;*/

			NSGAIIBuilder<DeploymentSolution> nsgaBuilder = new NSGAIIBuilder<DeploymentSolution>(problem, crossover, mutation);
			nsgaBuilder.setMaxEvaluations(1000);
			algorithm = nsgaBuilder.build();
			//AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
			//		.execute() ;
			algorithm.run();
			population = algorithm.getResult() ;
			Collections.sort(population, new RankingAndCrowdingDistanceComparator<>());
			int j = 0;
			for(int i = 0; i < population.size() && j < 5; i++)
			{
				if(!deployments.contains(population.get(i).getDeployment())
						&& problem.numberOfViolatedConstraints.getAttribute(population.get(i)) == 0
						&& isComplete(population.get(i).getDeployment()))
				{
					deployments.add(population.get(i).getDeployment());
					j++;
				}
			}
		}
		catch(Throwable T){
			System.err.println("Selection Error");
			population = algorithm.getResult() ;
			for(int i = 0; i < population.size(); i++)
			{
				if(!deployments.contains(population.get(i).getDeployment())
						&& problem.numberOfViolatedConstraints.getAttribute(population.get(i)) == 0)
					deployments.add(population.get(i).getDeployment());
			}
			return deployments;
		}
		return deployments;
	}


	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

}
