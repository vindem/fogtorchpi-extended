package at.ac.tuwien.ec.utils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Callable;

import at.ac.tuwien.ec.algorithms.HeftBatteryResearch;
import at.ac.tuwien.ec.algorithms.HeftCostResearch;
import at.ac.tuwien.ec.algorithms.HeftEchoResearch;
import at.ac.tuwien.ec.algorithms.HeftResearch;
import at.ac.tuwien.ec.algorithms.MAXBattery;
import at.ac.tuwien.ec.algorithms.MINCostResearch;
import at.ac.tuwien.ec.algorithms.MOHeftResearch;
import at.ac.tuwien.ec.algorithms.MinMinResearch;
import at.ac.tuwien.ec.algorithms.WeightedFunctionResearch;
import at.ac.tuwien.ec.algorithms.mo.edgeplanning.MOEdgePlanning;
import at.ac.tuwien.ec.algorithms.mo.scheduling.NSGAIIIResearch;
import at.ac.tuwien.ec.appmodel.*;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;

public class WorkloadSearch implements Callable<ArrayList<Deployment>>{
	
	private ArrayList<MobileApplication> workload;
	private String chosenAlgorithm;
	private MobileApplication currentApp;
	private MobileCloudInfrastructure currentInfrastructure;
		
	public WorkloadSearch(){
		
	}
	
	public WorkloadSearch(ArrayList<MobileApplication> workload){
		this.workload = workload;
	}
	
	public WorkloadSearch(ArrayList<MobileApplication> workload, String chosenAlgorithm){
		this.workload = workload;
		this.chosenAlgorithm = chosenAlgorithm;
	}
	
	public ArrayList<Deployment> findDeployments()
	{
		return findDeployments(this.currentInfrastructure, this.currentApp, this.chosenAlgorithm);
	}
	
	public ArrayList<Deployment> findDeployments(MobileCloudInfrastructure I, MobileApplication app, String algorithm){
		MobileApplication currentApp;
		Search singleSearch = null;
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();

		Deployment dep = new Deployment();

		currentApp = app;
		SimulationConstants.logger.info("Application: " + currentApp.getClass().getSimpleName());
		//SimulationConstants.logger.warning(I.toString());
		switch(algorithm){
		case "weighted":
			singleSearch = new HeftEchoResearch(currentApp, I);
			break;
		case "mincost":
			singleSearch = new MINCostResearch(currentApp, I);
			break;
		case "maxbatt":
			singleSearch = new MAXBattery(currentApp, I);
			break;
		case "minmin":
			singleSearch = new MinMinResearch(currentApp, I);
			break;
		case "heft":
			singleSearch = new HeftResearch(currentApp, I);
			break;
		case "hbatt":
			singleSearch = new HeftBatteryResearch(currentApp, I);
			break;
		case "hcost":
			singleSearch = new HeftCostResearch(currentApp, I);
			break;
		case "echo":
			singleSearch = new HeftEchoResearch(currentApp,I);
			break;
		case "moheft":
			singleSearch = new MOHeftResearch(currentApp,I);
			break;
		case "nsgaIII":
			singleSearch = new NSGAIIIResearch(currentApp,I);
			break;
		case "moplan":
			singleSearch = new MOEdgePlanning(currentApp, I);
			break;
		default:
			singleSearch = new MinMinResearch(currentApp, I, null);
		}
		deployments = singleSearch.findDeployments(dep);
		return deployments;
	}
	
	private boolean outliersDetected(Deployment dep) {
		if(dep.mobileEnergyBudget < 0.0
				|| dep.runTime <= 0.001
				|| dep.deploymentMonthlyCost.getCost() < 0.0)
			return true;
		return false;
	}

	public String getChosenAlgorithm() {
		return chosenAlgorithm;
	}

	public void setChosenAlgorithm(String chosenAlgorithm) {
		this.chosenAlgorithm = chosenAlgorithm;
	}

	public MobileApplication getCurrentApp() {
		return currentApp;
	}

	public void setCurrentApp(MobileApplication currentApp) {
		this.currentApp = currentApp;
	}

	public MobileCloudInfrastructure getCurrentInfrastructure() {
		return currentInfrastructure;
	}

	public void setCurrentInfrastructure(MobileCloudInfrastructure currentInfrastructure) {
		this.currentInfrastructure = currentInfrastructure.clone();
	}

	@Override
	public ArrayList<Deployment> call() throws Exception {
		return findDeployments(currentInfrastructure, currentApp, chosenAlgorithm);
	}
	
	
}

