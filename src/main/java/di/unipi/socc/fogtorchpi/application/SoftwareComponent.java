package di.unipi.socc.fogtorchpi.application;

import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import di.unipi.socc.fogtorchpi.utils.Software;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;


/**
 *
 * @author stefano
 */
public class SoftwareComponent implements Comparable {
    String identifier;
    List<Software> softwareReqs;
    Hardware hw;
    public ArrayList<ThingRequirement> Theta;
	private double MIPS;
	private double nodeRank;
	protected boolean visited = false;
	protected double rank;
	/* average instruction size in gigabytes (conversion from byte,
	 *  data coming from https://www.strchr.com/x86_machine_code_statistics)
	 */
	protected final double AVERAGE_INSTRUCTION_LENGTH = 5.0;

    public SoftwareComponent(String identifier, List<String> softwareReqs, Hardware hardwareReqs, ArrayList<ThingRequirement> Theta){
        this.identifier = identifier;
        this.setSoftware(softwareReqs);
        this.Theta = new ArrayList<>(Theta);
        this.hw = new Hardware(hardwareReqs);
    }
    
    public SoftwareComponent(String id){
        this.identifier = id;
    }
    
    public void setSoftware(Collection<String> software){
        this.softwareReqs = new ArrayList<>();
        for (String s : software){
            boolean add = this.softwareReqs.add(new Software(s));
        }
    }

    public List<Software> getSoftwareRequirements() {
        return softwareReqs;
    }

    public Hardware getHardwareRequirements() {
        return hw;
    }

    public String getId() {
        return this.identifier;
    }
    
    public Iterable<ThingRequirement> getThingsRequirements() {
        return this.Theta;
    }
    
    @Override
	public String toString(){
	    String result = "<";
	    result = result + this.identifier + ", " + this.softwareReqs + ", "+ hw + ", "+  Theta;        
	    result += ">";
	    return result; 
	}

	@Override
    public boolean equals(Object o){
        SoftwareComponent s2 = (SoftwareComponent) o;
        return this.identifier.equals(s2.getId());
    }

    @Override
    public int compareTo(Object o) {
        SoftwareComponent s2 = (SoftwareComponent) o;
        return this.identifier.compareTo(s2.identifier);
    }

	public double getRuntimeOnNode(ComputationalNode n, MobileCloudInfrastructure I) {
		double localRuntime = this.MIPS / (n.getMipsPerCore());
		return (n.isMobile())? localRuntime :
			localRuntime + I.getTransmissionTime((MobileSoftwareComponent)this, I.getMobileDevice().getId(), n.getId());//offloadAndDownloadTime(this,n,I);
	}
	
    
	/*private double offloadAndDownloadTime(SoftwareComponent softwareComponent, ComputationalNode n, MobileCloudInfrastructure I) {
		Couple c = new Couple(I.getMobileDevice().getId(),n.getId());
		Couple c1 = new Couple(n.getId(),I.getMobileDevice().getId());
		MobileSoftwareComponent msc = (MobileSoftwareComponent) this;
		if(I.L.containsKey(c) || I.L.containsKey(c1)){
			QoSProfile q = I.L.get(c);
			QoSProfile q1 = I.L.get(c1);
			double bw = Double.isFinite(q.getBandwidth())? q.getBandwidth() : q1.getBandwidth();
			double latency = Double.isFinite(q.getLatency())? q.getLatency() : q1.getLatency();
			//double latency = Double.isFinite(q.getBandwidth())? q.getBandwidth() : q1.getBandwidth();
			if(latency == Integer.MAX_VALUE)
				return Double.MAX_VALUE;
			double dataSize = msc.getOffloadData();
			//double offloadTime = ((dataSize + msc.getOutData()) / (bw * 8e+6))
			//		+ (((dataSize + msc.getOutData())/65535) * (latency/1000));
			double offloadTime = ((dataSize + msc.getOutData()) / (bw * 125000.0))
					+ (latency/1000.0) * SimulationConstants.offloadable_part_repetitions;
			return offloadTime;
		}
		return Double.MAX_VALUE;
	}*/

	public void setMips(double mips){
		this.MIPS = mips;
	}
	
	public double getMips(){
		return this.MIPS;
	}

	public double getLocalRuntimeOnNode(ComputationalNode n, MobileCloudInfrastructure i) {
		return this.MIPS / n.getMipsPerCore();
	}
	
	public double getNodeRank()
	{
		return nodeRank;
	}
	
	public void setNodeRank(double rank)
	{
		nodeRank = rank;
	}
	
	public void setVisited(boolean b) {
		this.visited = b;		
	}

	public boolean isVisited() {
		return visited;
	}
	
}
