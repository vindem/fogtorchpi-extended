package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

import at.ac.tuwien.ec.algorithms.utils.NodeRankComparator;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class HeftBatteryResearch extends DAGResearch {

	public HeftBatteryResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(A.S.size(), new NodeRankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		
		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0.0;
		
		while(!isComplete(deployment)){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
			}
			ComputationalNode target = null;
			double maxB = Double.MIN_VALUE;
			MobileSoftwareComponent currTask = tasks.poll();
			if(!currTask.isOffloadable())
			{ 
					if(isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
					{
						target = I.getMobileDevice(currTask.getUid());
						scheduledNodes.add(currTask);
					}
					else
						deployment = null;
			}
			else
			{
				if( I.getMobileDevice(currTask.getUid()).getEnergyBudget() - I.getMobileDevice(currTask.getUid()).getCPUEnergyModel().computeCPUEnergy(currTask, I.getMobileDevice(currTask.getUid()), I) > maxB
						&& isValid(deployment, currTask, I.getMobileDevice(currTask.getUid())))
				{
					maxB = deployment.energyConsumption - I.getMobileDevice(currTask.getUid()).getCPUEnergyModel().computeCPUEnergy(currTask, I.getMobileDevice(currTask.getUid()), I);
					target = I.getMobileDevice(currTask.getUid());
				}
				for(CloudDatacentre cdc : I.C.values())
					if( I.getMobileDevice(currTask.getUid()).getEnergyBudget() - I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, cdc, I) > maxB
							&& isValid(deployment, currTask, cdc))
					{
						maxB = deployment.energyConsumption - I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, cdc, I);
						target = cdc;
					}
				for(FogNode fn : I.F.values())
					if( I.getMobileDevice(currTask.getUid()).getEnergyBudget() - I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, fn, I) > maxB
							&& isValid(deployment, currTask, fn))
					{
						maxB = deployment.energyConsumption - I.getMobileDevice(currTask.getUid()).getNetEnergyModel().computeNETEnergy(currTask, fn, I);
						target = fn;
					}
			}
			if(target!=null)
			{
				deploy(deployment,currTask,target);
				setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
				scheduledNodes.add(currTask);
				//tasks.remove();
			}
			else
			{
				if(scheduledNodes.isEmpty())
					return null;
			}
		}
		while(!scheduledNodes.isEmpty())
		{
			MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
			currentRuntime = firstTaskToTerminate.getRuntime();
			deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
			scheduledNodes.remove(firstTaskToTerminate);
		}
		//deployment.runTime += currentRuntime;
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		deployments.add(deployment);
		return deployments;
	}
	
	
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
