package at.ac.tuwien.ec.edgeoffload;

import static java.util.Arrays.asList;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.opencsv.CSVReader;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.mobileapps.AntivirusApp;
import at.ac.tuwien.ec.appmodel.mobileapps.ChessApp;
import at.ac.tuwien.ec.appmodel.mobileapps.FaceRecognizerApp;
import at.ac.tuwien.ec.appmodel.mobileapps.FacebookApp;
import at.ac.tuwien.ec.appmodel.mobileapps.NavigatorApp;
import at.ac.tuwien.ec.appmodel.mobileapps.NavigatorAppExp;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants.Timezone;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.MobileDevice;
import at.ac.tuwien.ec.infrastructuremodel.energy.AMDCPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.Mobile3GNETEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.MobileWiFiNETEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.SamsungS2DualEnergyModel;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Coordinates;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;



public class Helper {
	
	public static HashMap<Timezone,Double[]> timezoneData;
	public static final Timezone[] timezones = {
			SimulationConstants.Timezone.INDIANAPOLIS,
			SimulationConstants.Timezone.DETROIT,
			SimulationConstants.Timezone.DUBLIN,
			SimulationConstants.Timezone.STGHISLAIN,
			SimulationConstants.Timezone.SINGAPORE,
			SimulationConstants.Timezone.KOREA,
			SimulationConstants.Timezone.INDIANAPOLIS,
			SimulationConstants.Timezone.DETROIT,
			SimulationConstants.Timezone.SINGAPORE,
			SimulationConstants.Timezone.KOREA
		};
	
	public Helper()
	{
		timezoneData = setupTimezones(SimulationConstants.traceFile);
		System.out.println("Timezone data... loaded");
	}
	public ArrayList<MobileApplication> setupWorkload(int appExecutions, String mobileId){
		ArrayList<MobileApplication> workload = new ArrayList<MobileApplication>();
		double appF = SimulationConstants.rand.nextDouble();
		String sApp;
			
		for(int i = 0; i < appExecutions; i++)
		{
			sApp = drawApp();
			switch(sApp){
			case "NAVI":
				workload.add(new NavigatorAppExp(i,mobileId));
				break;
			case "CHESS":
				workload.add(new ChessApp(i,mobileId));
				break;
			case "ANTIVIRUS":
				workload.add(new AntivirusApp(i,mobileId));
				break;
			case "FACEREC":
				workload.add(new FaceRecognizerApp(i,mobileId));
				break;
			case "FACEBOOK":
				workload.add(new FacebookApp(i,mobileId));
				break;
			}
			
		}
		
		return workload;
    }

	private static String drawApp() {
		double appF = SimulationConstants.rand.nextDouble();
		if(appF >= 0 && appF <= 0.45)
			return "FACEBOOK";
		if(appF > 0.45 && appF <= 0.75)
			return "NAVI";
		if(appF > 0.75 && appF <= 0.85)
			return "FACEREC";
		if(appF > 0.85 && appF <= 0.95)
			return "CHESS";
		if(appF > 0.95 && appF <= 1.0)
			return "ANTIVIRUS";
		return null;
	}

	public MobileCloudInfrastructure setupInfrastructure(){
        MobileCloudInfrastructure I = new MobileCloudInfrastructure();
        //int cloudCoreNum = (int) ExponentialDistributionGenerator.getNext(1/512.0);
        int cloudCoreNum = 256;
        
        int cloudNodes = (SimulationConstants.cloudOnly)? 
        		SimulationConstants.cloudNodes + SimulationConstants.edgeNodes : 
        		SimulationConstants.cloudNodes;
        int edgeNodes = (SimulationConstants.cloudOnly)? 
        		0 :	SimulationConstants.edgeNodes;
        
                        
        for(Timezone tz : timezoneData.keySet())
        {
        	Double[] tzTraces = timezoneData.get(tz);       
        	I.addPrices(new Coordinates(tz.getX(),tz.getY()),tzTraces);
        }
               
        for(int i = 0; i < cloudNodes; i++)
        	I.addCloudDatacentre("cloud_"+i, asList(
        		new Couple<String,Double>("spark",0.0),
        		new Couple<String,Double>("mySQL",0.0),
        		new Couple<String,Double>("linux",0.0),
        		new Couple<String,Double>("windows",0.0),
        		new Couple<String,Double>("python",0.0),
        		new Couple<String,Double>("c++",0.0),
        		new Couple<String,Double>(".NETcore",0.0)),
        			timezones[i].getX(),
        			timezones[i].getY(),
        		new Hardware(cloudCoreNum, 128.0, (int)10e12, 0.03, 0.02, 0.01),
        		new AMDCPUEnergyModel(),
        		2000);
        
        
        //Mobile nodes
        for(int i = 0; i < SimulationConstants.mobileNum; i++)
        {
        	MobileDevice device;
        	device = new MobileDevice("mobile_"+i,
        			asList(new Couple<String,Double>("python",0.0)),
        			new Hardware(2,16,(int)16e10),
        			SimulationConstants.rand.nextInt(SimulationConstants.MAP_M),
        			SimulationConstants.rand.nextInt(SimulationConstants.MAP_N));
        	device.setCPUEnergyModel(new SamsungS2DualEnergyModel());
        	device.setMipsPerCore(600);
        	device.setEnergyBudget(SimulationConstants.batteryBudget);
        	I.addMobileDevice(device);
        }
                
        for(String mId : I.M.keySet()){
        	//Qos Profiles
        	double Cloud3GBandwidth = ExponentialDistributionGenerator.getNext(1.0/3.6);
        	double CloudWiFiHQBandwidth = ExponentialDistributionGenerator.getNext(1.0/16.0);
        	double CloudWiFiLQBandwidth = ExponentialDistributionGenerator.getNext(1.0/2.0);

        	double cloudLatency = ((SimulationConstants.rand.nextGaussian() * 33.5) + 200);
        	QoSProfile qos3gUPCloud = new QoSProfile(asList(
        			new Couple<QoS,Double>(new QoS(54.0 + cloudLatency, Cloud3GBandwidth), 0.9957),
        			new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0043)));
        	QoSProfile qos3gDNCloud = new QoSProfile(asList(
        			new Couple<QoS,Double>(new QoS(54.0 + cloudLatency, Cloud3GBandwidth), 0.9959),
        			new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0041)));
        	QoSProfile qosWifiUPCloud = new QoSProfile(asList(
        			new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, CloudWiFiHQBandwidth), 0.9),
        			new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, CloudWiFiLQBandwidth), 0.09),
        			new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)));
        	QoSProfile qosWifiDNCloud = new QoSProfile(asList(
        			new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, CloudWiFiHQBandwidth), 0.9),
        			new Couple<QoS,Double>(new QoS(15.0 + cloudLatency, CloudWiFiLQBandwidth), 0.09),
        			new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
        			));
        	QoSProfile qosDownCloud,qosUpCloud;
        	boolean wifi = SimulationConstants.rand.nextDouble() < 0.1;
        	qosUpCloud = (wifi)? qosWifiUPCloud : qos3gUPCloud;
        	qosDownCloud = (wifi)? qosWifiDNCloud : qos3gDNCloud;
        	//Links
        	I.getMobileDevice(mId).setNetEnergyModel((wifi)? new MobileWiFiNETEnergyModel() : new Mobile3GNETEnergyModel());
        	for(int i = 0; i < cloudNodes; i++)
        	{
        		I.addLink(mId, "cloud_"+i,qosUpCloud,qosDownCloud);
        		I.addLink("cloud_"+i, mId,qosUpCloud,qosDownCloud);
        	}
        }
        
        //Fog nodes
        boolean wifi = SimulationConstants.rand.nextDouble() < 0.1;
        if(!SimulationConstants.cloudOnly)	//for(int i = 0; i < edgeNodes; i++)
        	I.setupEdgeNodes(SimulationConstants.edgeCoreNum, timezoneData, SimulationConstants.edgePlanningAlgorithm,wifi);
       //System.out.println("Setup infrastructure with " + I.C.size() + " Cloud nodes and " + I.F.size() + " Edge nodes.");
       return I;
    }

	private static HashMap<Timezone,Double[]> setupTimezones(String fileUrl) {
		ArrayList<Double[]> tz = new ArrayList<Double[]>();
		HashMap<String,ArrayList<String>> fTraces = new HashMap<String,ArrayList<String>>();
		HashMap<Timezone,Double[]> timezoneData = new HashMap<Timezone,Double[]>();
		Double[] doubleTraces;
		try {
			FileReader filereader = new FileReader(fileUrl);
			CSVReader csvReader = new CSVReader(filereader);
			String nextRecord[];
			csvReader.readNext();
			while((nextRecord = csvReader.readNext())!=null)
			{
				if(fTraces.containsKey(nextRecord[1]))
					fTraces.get(nextRecord[1]).add(nextRecord[3]);
				else
				{
					ArrayList<String> prices = new ArrayList<String>();
					prices.add(nextRecord[3]);
					fTraces.put(nextRecord[1], prices);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Timezone[] zones = new Timezone[6];
		zones[0] = SimulationConstants.Timezone.INDIANAPOLIS;
		zones[1] = SimulationConstants.Timezone.DETROIT;
		zones[2] = SimulationConstants.Timezone.DUBLIN;
		zones[3] = SimulationConstants.Timezone.STGHISLAIN;
		zones[4] = SimulationConstants.Timezone.SINGAPORE;
		zones[5] = SimulationConstants.Timezone.KOREA;
		
		int j = 0;
		for(String key : fTraces.keySet())
		{
			if(j >= zones.length)
				break;
			doubleTraces = new Double[fTraces.get(key).size()];
			for(int i = 0; i < doubleTraces.length; i++)
				doubleTraces[i] = (Double.parseDouble(fTraces.get(key).get(i)));
			
			timezoneData.put(zones[j],doubleTraces);
			j++;
		}
		return timezoneData;
	}
}
