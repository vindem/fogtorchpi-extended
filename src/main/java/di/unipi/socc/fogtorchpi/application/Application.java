package di.unipi.socc.fogtorchpi.application;

import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;


/**
 *
 * @author stefano
 */
public class Application {
    public ArrayList<SoftwareComponent> S;
    public LinkedHashMap<Couple<String,String>, QoSProfile> L;
    
    public Application(){
        S = new ArrayList<>();
        L = new LinkedHashMap<>();
    }

    public void addLink(String a, String b, double latency, double bandwidth) {
        L.put(new Couple<String,String>(a,b), new QoSProfile(latency,bandwidth));
        //L.put(new Couple(b,a), new QoSProfile(latency,bandwidth));
    }

    public void addLink(String a, String b, double latency, double downlink, double uplink) {
        L.put(new Couple<String,String>(a,b), new QoSProfile(latency,uplink));
        //L.put(new Couple(b,a), new QoSProfile(latency,downlink));
    }

    public void addComponent(String id, List<String> softwareReqs, Hardware hardwareReqs, ArrayList<ThingRequirement> Theta) {
        S.add(new SoftwareComponent(id, softwareReqs, hardwareReqs, Theta));   
    }
    
    public void addComponent(String id, List<String> softwareReqs, Hardware hardwareReqs) {
        S.add(new SoftwareComponent(id, softwareReqs, hardwareReqs, new ArrayList<>()));   
    }
    
    public void removeComponent(SoftwareComponent sc){
    	S.remove(sc);
    }
    
    public void removeLink(Couple link){
    	L.remove(link);
    }
    
    public ArrayList<Couple<String,String>> incidentEdgesIn(SoftwareComponent sc){
    	ArrayList<Couple<String,String>> incidentEdges = new ArrayList<Couple<String,String>>();
    	for(SoftwareComponent component: S){
    		if(sc.getId().equals(component.getId()))
    			continue;
    		Couple<String,String> edge 
    			= new Couple<String,String>(component.getId(),sc.getId());
    		if(L.containsKey(edge))
    			incidentEdges.add(edge);
    	}
    	return incidentEdges;
    }
    
    public ArrayList<Couple<String,String>> outgoingEdgesFrom(SoftwareComponent sc){
    	ArrayList<Couple<String,String>> outgoingEdges = new ArrayList<Couple<String,String>>();
    	for(SoftwareComponent component: S){
    		if(sc.getId().equals(component.getId()))
    			continue;
    		Couple<String,String> edge 
    			= new Couple<String,String>(sc.getId(),component.getId());
    		if(L.containsKey(edge))
    			outgoingEdges.add(edge);
    	}
    	return outgoingEdges;
    }
    
    @Override
    public String toString(){
        String result = "S = {\n";
        
        for (SoftwareComponent s: S){
            result+="\t"+s;
            result+="\n";
        }
        
        result+="}\n\nLambda = {\n";
        
        for (Couple c : L.keySet()){
            result+="(" + c + "\t"+ L.get(c) + ")";
            result+="\n";
        }
        
        result+="}";
        
        return result;
    }
    
    public void computeNodeRanks(MobileCloudInfrastructure I)
    {
    	for(SoftwareComponent sc : S)
    		sc.setVisited(false);
    	
    	upRank((MobileSoftwareComponent)S.get(0),(MobileApplication) this,I);
    }
    
    private void upRank(SoftwareComponent cmp, MobileApplication A,MobileCloudInfrastructure I) {
		double w_cmp;
		
		if(!cmp.isVisited())
		{
			cmp.setVisited(true);
			double nNodes = I.C.size() + I.F.size() + 1.0;
			double rtSums = cmp.getLocalRuntimeOnNode(I.getMobileDevice(), I);
			for(CloudDatacentre cdc : I.C.values())
				rtSums += cmp.getLocalRuntimeOnNode(cdc, I);
			for(FogNode fn : I.F.values())
				rtSums += cmp.getLocalRuntimeOnNode(fn, I);
			w_cmp = rtSums / nNodes;

			ArrayList<MobileSoftwareComponent> neighbors = A.getNeighbors(cmp);
			if(neighbors.isEmpty())
				cmp.setNodeRank(w_cmp);
			else 
			{
				for(MobileSoftwareComponent neighbor : neighbors )
					upRank(neighbor, A, I);

				double tmpWRank;
				double maxSuccRank = Double.MIN_VALUE;
				for(MobileSoftwareComponent neighbor : neighbors ){
					tmpWRank = neighbor.getNodeRank();

					double tmpCRank = 1.0;
					if(neighbor.isOffloadable())
					{
						for(CloudDatacentre cdc : I.C.values())
							tmpCRank += I.getTransmissionTime(neighbor, I.getMobileDevice().getId(), cdc.getId());
						for(FogNode fn : I.F.values())
							tmpCRank += I.getTransmissionTime(neighbor, I.getMobileDevice().getId(), fn.getId());
						tmpCRank = tmpCRank / (I.C.size() + I.F.size() + 1);
					}
					double tmpRank = tmpCRank + tmpWRank;
					maxSuccRank = (tmpRank > maxSuccRank)? tmpRank : maxSuccRank ;		
				}
				cmp.setNodeRank(w_cmp + maxSuccRank);
			}

		}
    }
}
