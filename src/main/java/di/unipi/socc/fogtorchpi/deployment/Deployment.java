/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.unipi.socc.fogtorchpi.deployment;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Cost;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

import java.text.DecimalFormat;
import java.util.TreeMap;


import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;

/**
 *
 * @author stefano
 */
public class Deployment extends LinkedHashMap<SoftwareComponent, ComputationalNode>{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5978753101322855324L;
	HashMap<String, HashSet<String>> businessPolicies;
    public Couple<Double, Double> consumedResources;
    public Cost deploymentMonthlyCost;
	public double energyConsumption;
	public double mobileEnergyBudget;
	public double energyCost;
	public double runTime;
    
    public Deployment(){
        super();
        businessPolicies = new HashMap<>();   
        deploymentMonthlyCost = new Cost(0);
        energyConsumption = 0.0;
        energyCost = 0.0;
        mobileEnergyBudget = SimulationConstants.batteryCapacity;
        runTime = 0.0;
    }

    Deployment(Deployment deployment) {
        super(deployment);
    }
    
    @Override
    public Object clone(){
        Deployment d = new Deployment (this);
        d.deploymentMonthlyCost = new Cost(deploymentMonthlyCost.getCost());
        d.consumedResources = this.consumedResources;
        d.businessPolicies = this.businessPolicies;
        d.energyConsumption = this.energyConsumption;
        d.mobileEnergyBudget = this.mobileEnergyBudget;
        d.energyCost = this.energyCost;
        d.runTime = this.runTime;
        return d;
    }
    
    @Override
    public String toString(){
        String result ="";

        for (SoftwareComponent s : super.keySet()){
            result+="["+s.getId()+"->" +super.get(s).getId()+"]" ;
        }
        
        return result;   
    }
    
    @Override
    public boolean equals(Object o){
        boolean result = true;
        Deployment d = (Deployment) o;
        for (SoftwareComponent s : this.keySet()){
            if (!this.get(s).equals(d.get(s))){
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        String s = this.toString();
        hash = 47 * hash + s.hashCode();
        return hash;
    }

    public void addRuntime(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure I){
    	double tmp = s.getRuntimeOnNode(n, I);
    	if(this.runTime < tmp)
    		this.runTime = tmp;
    }
    
    public void removeRuntime(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure I){
    	this.runTime -= s.getRuntimeOnNode(super.get(s), I);
    }
    
    public void addCost(SoftwareComponent s, ComputationalNode n, Infrastructure I) {
        this.deploymentMonthlyCost.add(n.computeCost(s, I));
    }
    
    public void removeCost(SoftwareComponent s, ComputationalNode n, Infrastructure I){
        this.deploymentMonthlyCost.remove(n.computeCost(s, I));
    }

	public void addEnergyConsumption(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		if(n.isMobile())
		{
			i.getMobileDevice().removeFromBudget(computeCPUEnergyConsumption(s, n, i));
			this.mobileEnergyBudget = i.getMobileDevice().getEnergyBudget();
		}
		else
		{
			i.getMobileDevice().removeFromBudget(computeOffloadEnergy(s,n,i));
			this.energyConsumption += computeCPUEnergyConsumption(s,n,i);
			this.energyCost += computeEnergyCost(s,n,i);
			this.mobileEnergyBudget = i.getMobileDevice().getEnergyBudget();
		}
	}

	private double computeEnergyCost(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		return computeCPUEnergyConsumption(s, n, i) * n.getEnergyCost();
	}

	private double computeOffloadEnergy(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		return i.getMobileDevice().getNetEnergyModel().computeNETEnergy(s, n, i);
	}

	private double computeCPUEnergyConsumption(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		CPUEnergyModel model = n.getCPUEnergyModel();
		return model.computeCPUEnergy(s,n,i);
	}

	public void removeEnergyConsumption(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		if(n.isMobile()){
			i.getMobileDevice().addToBudget(computeCPUEnergyConsumption(s, n, i));
			this.mobileEnergyBudget = i.getMobileDevice().getEnergyBudget();
		}
		else
		{
			i.getMobileDevice().addToBudget(computeOffloadEnergy(s,n,i));
			this.energyConsumption -= computeCPUEnergyConsumption(s,n,i);
			this.mobileEnergyBudget = i.getMobileDevice().getEnergyBudget();
		}
	}

}
