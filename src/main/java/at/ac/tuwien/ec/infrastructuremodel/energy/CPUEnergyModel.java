package at.ac.tuwien.ec.infrastructuremodel.energy;

import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;

public interface CPUEnergyModel {

	public double computeCPUEnergy(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i);
	public double getIdlePower(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i);
}
