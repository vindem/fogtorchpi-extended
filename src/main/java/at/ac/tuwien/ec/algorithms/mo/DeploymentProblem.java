package at.ac.tuwien.ec.algorithms.mo;

import java.util.ArrayList;
import java.util.Collections;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.PermutationProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.problem.impl.AbstractIntegerPermutationProblem;
import org.uma.jmetal.solution.PermutationSolution;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import at.ac.tuwien.ec.algorithms.DAGResearch;
import at.ac.tuwien.ec.algorithms.RandomDeploymentSearch;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class DeploymentProblem extends DAGResearch implements ConstrainedProblem<DeploymentSolution>
{
	/**
	 * 
	 */
	public OverallConstraintViolation<DeploymentSolution> overallConstraintViolationDegree = 
			new OverallConstraintViolation<DeploymentSolution>();
	public NumberOfViolatedConstraints<DeploymentSolution> numberOfViolatedConstraints =
			new NumberOfViolatedConstraints<DeploymentSolution>();
	
	private static final long serialVersionUID = 1L;
		
	
	public DeploymentProblem(MobileCloudInfrastructure I, MobileApplication A)
	{
		super(A,I);
	}
	
	@Override
	public DeploymentSolution createSolution() {
		Deployment dep = new Deployment();
		RandomDeploymentSearch rs = new RandomDeploymentSearch(A,I);
		ArrayList<Deployment> deps;
		do deps = rs.findDeployments(dep);
		while(deps.size()==0);
		return new DeploymentSolution(deps.get(0),A,I);
	}

	@Override
	public void evaluate(DeploymentSolution currDep) {
		double cost = 0;
		double batteryBudget = SimulationConstants.batteryCapacity;
		Deployment d = currDep.getDeployment();
		double runTime = 0.0;
		for(int i = 0; i < d.keySet().size(); i++){
			MobileSoftwareComponent sc = (MobileSoftwareComponent) d.keySet().toArray()[i];
			ComputationalNode n = currDep.getVariableValue(i);
			ArrayList<MobileSoftwareComponent> preds = A.getPredecessors(sc);
			double mPreds = 0.0;
			for(MobileSoftwareComponent ps:preds)
			{
				if(ps.getRuntime() > mPreds)
					mPreds = ps.getRuntime();
			}
			sc.setRuntime(mPreds + sc.getRuntimeOnNode(n, I));
			runTime = sc.getRuntime();
			cost += n.computeCost(sc, I).getCost();
			batteryBudget -= (n.isMobile())? 
					I.getMobileDevice().getCPUEnergyModel().computeCPUEnergy(sc, n, I):
					I.getMobileDevice().getNetEnergyModel().computeNETEnergy(sc, n, I);	
		}
		currDep.setObjective(0, runTime);
		currDep.setObjective(1, cost);
		currDep.setObjective(2, batteryBudget);
	}

	@Override
	public void evaluateConstraints(DeploymentSolution arg0) {
		int violatedConstraints = 0;
		double overAllConstraintViolation = 0.0;

		Object[] comps = arg0.getDeployment().keySet().toArray();

		boolean rankConstraintViolation = false;
		for(int i = 0; i < comps.length - 1; i++)
		{
			if(((MobileSoftwareComponent)comps[i]).getNodeRank() < ((MobileSoftwareComponent)comps[i+1]).getNodeRank())
			{
				if(!rankConstraintViolation){
					rankConstraintViolation = true;
					violatedConstraints++;
				}
				overAllConstraintViolation -= 10;
				break;
			}
		}
		
		Deployment temp = new Deployment();
		boolean hardwareConstraintViolation = false;
		for(int i = 0; i < arg0.getNumberOfVariables(); i++)
		{
			ComputationalNode cn = arg0.getVariableValue(i);
			MobileSoftwareComponent msc = (MobileSoftwareComponent) comps[i];
			
			Hardware cnHardware = cn.getHardware();
			if(!isValid(temp,msc,cn))
			{
				if(!hardwareConstraintViolation)
				{
					hardwareConstraintViolation = true;
					violatedConstraints++;					
				}
				overAllConstraintViolation += cn.getHardware().cores - msc.getHardwareRequirements().cores;
				break;
			}
			//else
				//deploy(temp,msc,cn);
		}
		
		for(int i = 0; i < arg0.getNumberOfVariables(); i++)
		{
			ComputationalNode cn = arg0.getVariableValue(i);
			MobileSoftwareComponent msc = (MobileSoftwareComponent) comps[i];
			undeploy(temp,msc,cn);
		}
		
		boolean offloadabilityConstraintViolation = false;
		for(int i = 0; i < arg0.getNumberOfVariables(); i++)
		{
			MobileSoftwareComponent msc = ((MobileSoftwareComponent)comps[i]);
			ComputationalNode cn = arg0.getDeployment().get(msc);
			if(!msc.isOffloadable() && !(cn.equals(I.getMobileDevice())))
			{
				if(!offloadabilityConstraintViolation)
				{
				violatedConstraints++;
				offloadabilityConstraintViolation = true;
				}
				overAllConstraintViolation -= 100.0;
			}
		}
		
		if(arg0.getDeployment().mobileEnergyBudget < 0.0)
		{
			violatedConstraints++;
			overAllConstraintViolation = arg0.getDeployment().mobileEnergyBudget;
		}
		
		if(!(Double.isFinite(arg0.getObjective(0)))
				|| !Double.isFinite(arg0.getObjective(1))
				|| !Double.isFinite(arg0.getObjective(2)))
		{
			violatedConstraints++;
		}
		numberOfViolatedConstraints.setAttribute(arg0, violatedConstraints);
		overallConstraintViolationDegree.setAttribute(arg0,overAllConstraintViolation);
	}

	@Override
	public String getName() {
		return "FirstHopDagOffloading";
	}

	@Override
	public int getNumberOfObjectives() {
		return 3;
	}

	@Override
	public int getNumberOfVariables() {
		// TODO Auto-generated method stub
		return A.S.size();
	}

	@Override
	public int getNumberOfConstraints() {
		return 6;
	}

	protected boolean isValid(Deployment deployment, MobileSoftwareComponent s, ComputationalNode n) {
        return n.isCompatible(s,I) && isOffloadPossibleOn(deployment, s, n)
        		&& checkLinks(deployment,s,n);
    }

	private boolean isOffloadPossibleOn(Deployment deployment, SoftwareComponent s, ComputationalNode n){
    	Couple<String,String> l1,l2;
    	l1 = new Couple<String,String>(I.getMobileDevice().getId(),n.getId());
    	l2 = new Couple<String,String>(n.getId(),I.getMobileDevice().getId());
    	if(I.L.containsKey(l1) && I.L.containsKey(l2))
    	{
    		QoSProfile q1 = I.L.get(l1);
    		QoSProfile q2 = I.L.get(l2);
    		return q1.getBandwidth() > 0 && q2.getBandwidth() > 0;
    	}
    	return false;
    }
	
	 private boolean checkLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
	    	for (SoftwareComponent c : deployment.keySet()) {
	    		ComputationalNode m = deployment.get(c); // nodo deployment c
	    		Couple couple1 = new Couple(c.getId(), s.getId());
	    		Couple couple2 = new Couple(s.getId(), c.getId());
	    		if (A.L.containsKey(couple1)) {
	    			QoSProfile req1 = A.L.get(couple1);
	    			QoSProfile req2 = A.L.get(couple2);
	    			Couple c1 = new Couple(m.getId(), n.getId());
	    			Couple c2 = new Couple(n.getId(), m.getId());
	    			//System.out.println("Finding a link for " + couple1 + " between " + c1);
	    			if (I.L.containsKey(c1)) {
	    				QoSProfile off1 = I.L.get(c1);
	    				QoSProfile off2 = I.L.get(c2);

	    				if (req1 != null && !off1.supports(req1) || req2 != null && !off2.supports(req2)) 
	    					return false;

	    				if(I.getTransmissionTime((MobileSoftwareComponent) s, I.getMobileDevice().getId(),
	    						deployment.get(c).getId()) > 
	    				I.getTransmissionTimeWithQoS((MobileSoftwareComponent) s, I.getMobileDevice().getId(),
	    						deployment.get(c).getId(), req1) )
	    					return false;
	    			} else {
	    				//System.out.println("It does not exist");
	    				return false;
	    			}
	    		}
	    	}
	    	return true;
	    }

	 	protected synchronized void deploy(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
	        SimulationConstants.logger.info("Adding " + s.getId() + " = " + deployment.containsKey(s));
	    	deployment.put(s, n);
	        //deployment.addRuntime(s, n, I);
	        //System.out.println(deployment + " " + deployment.size());
	        n.deploy(s);
	        deployLinks(deployment, s, n);
	    }

	    protected void undeploy(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
	        if (deployment.containsKey(s)) {
	        	n.undeploy(s);
	        	deployment.remove(s);
	            undeployLinks(deployment, s, n);
	        }
	       // System.out.println("UNDEP"+deployment);
	    }
	 
	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
