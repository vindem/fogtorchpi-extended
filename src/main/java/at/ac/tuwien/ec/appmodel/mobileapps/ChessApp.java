package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class ChessApp extends MobileApplication {

	private int movesNum = SimulationConstants.chessMoves;
	
	public ChessApp(){
		super(0);
		sampleTasks();
		sampleLinks();
	}
	
	public ChessApp(int id){
		super(id);
		sampleTasks();
		sampleLinks();
	}
	
	public ChessApp(int id, String uId){
		super(id,uId);
		sampleTasks();
		sampleLinks();
	}
	
	@Override
	public void sampleLinks() {
		for(int i = 0; i < movesNum - 1; i++)
		{
			addLink("CHESS_UI_"+i+"_"+getWorkloadId()+","+mobileId
					, "UPDATE_CHESS_"+i+"_"+getWorkloadId()+","+mobileId,
					sampleLatency(),
					0.1);
			addLink("UPDATE_CHESS_"+i+"_"+getWorkloadId()+","+mobileId,
					"COMPUTE_MOVE_"+i+"_"+getWorkloadId()+","+mobileId,
					sampleLatency(),
					0.1);
			addLink("COMPUTE_MOVE_"+i+"_"+getWorkloadId()+","+mobileId
					,"CHESS_OUTPUT_"+i+"_"+getWorkloadId()+","+mobileId,
					sampleLatency(),
					0.1);
			addLink("CHESS_OUTPUT_"+i+"_"+getWorkloadId()+","+mobileId,
					"CHESS_UI_"+(i+1)+"_"+getWorkloadId()+","+mobileId,
					sampleLatency(),
					0.1);
		}
		addLink("CHESS_UI_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId
				, "UPDATE_CHESS_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("UPDATE_CHESS_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId
				, "COMPUTE_MOVE_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("COMPUTE_MOVE_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId,
				"CHESS_OUTPUT_"+(movesNum-1)+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
	}

	@Override
	public void sampleTasks() {
		for(int i = 0; i < movesNum; i++)
		{
			addComponent("CHESS_UI_"+i+"_"+getWorkloadId()+","+mobileId,
					this.getMobileId(),
					asList("python"),
					new Hardware(1, 1, 1)
					,false
					//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/4.0) + 4.0)
					,4.0e3*SimulationConstants.task_multiplier
					,5e+3*SimulationConstants.task_multiplier
					,5e+3*SimulationConstants.task_multiplier
					);
			addComponent("UPDATE_CHESS_"+i+"_"+getWorkloadId()+","+mobileId,
					this.getMobileId(),
					asList("python"),
					new Hardware(1, 1, 1)
					//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/2.0) + 2.0)
					,2.0e3*SimulationConstants.task_multiplier
					,5e3*SimulationConstants.task_multiplier
					,5e3*SimulationConstants.task_multiplier
					);
			addComponent("COMPUTE_MOVE_"+i+"_"+getWorkloadId()+","+mobileId,
					this.getMobileId(),
					asList("python")
					,new Hardware(1,2,1)
					//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/SimulationConstants.chess_mi) + SimulationConstants.chess_mi)
					,SimulationConstants.chess_mi*SimulationConstants.task_multiplier					
					,5e+3*SimulationConstants.task_multiplier
					,5e+3*SimulationConstants.task_multiplier
					);
			addComponent("CHESS_OUTPUT_"+i+"_"+getWorkloadId()+","+mobileId,
					this.getMobileId(),
					asList("python"),
					new Hardware(1, 1, 1)
					,false
					//,Math.ceil(ExponentialDistributionGenerator.getNext(1.0/2.0)+1.0)
					,2.0e3*SimulationConstants.task_multiplier
					,5e3*SimulationConstants.task_multiplier
					,5e3*SimulationConstants.task_multiplier
					);
		}
	}

	@Override
	public MobileApplication clone() {
		ChessApp cloned = new ChessApp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = this.L;
		return cloned;
	}

}
