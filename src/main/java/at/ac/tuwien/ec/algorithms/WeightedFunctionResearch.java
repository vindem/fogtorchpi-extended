package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class WeightedFunctionResearch extends DAGResearch {
		
	private static final double gamma = SimulationConstants.gamma;
	private static final double alpha = SimulationConstants.alpha;
	private static final double beta = SimulationConstants.beta;

	public WeightedFunctionResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
		// TODO Auto-generated constructor stub
	}
	
	public WeightedFunctionResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	

	private double computeScore(SoftwareComponent s, ComputationalNode cn, MobileCloudInfrastructure i, double minRuntime, double minCost, double maxBattery) {
		//if(cn.isMobile())
		//	return Double.MAX_VALUE;
		double currRuntime = s.getRuntimeOnNode(cn, I);
		double currCost = cn.computeCost(s, I).getCost();
		double currBattery = I.getMobileDevice(s.getUid()).getEnergyBudget() - 
				((cn.isMobile())? cn.getCPUEnergyModel().computeCPUEnergy(s, cn, I) 
				: I.getMobileDevice(s.getUid()).getNetEnergyModel().computeNETEnergy(s, cn, I));
		
		double runtimeDiff = currRuntime - minRuntime;
		double costDiff = currCost - minCost;
		double batteryDiff = maxBattery - currBattery;
		double minRange,maxRange;
				
		if(runtimeDiff < costDiff && runtimeDiff < batteryDiff)
			minRange = runtimeDiff;
		
		return alpha * adjust(runtimeDiff,1)  + beta * adjust(costDiff,1.0) + gamma * adjust(batteryDiff,1.0);
	}

	private double normalized(double x, double minRange, double maxRange) {
		return (x - minRange) / (maxRange - minRange) ;
	}
	
	private double adjust(double x, double factor){
		return x * factor;
	}

	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		//if (K.get(msc.getId()) != null) 
		//{
			if(!msc.isOffloadable())
				if(isValid(deployment,msc,I.getMobileDevice(msc.getUid())))
					return I.getMobileDevice(msc.getUid());
				else
					return null;
			
			ArrayList<ComputationalNode> compNodes = compatibleNodes(deployment,msc);
			ArrayList<ComputationalNode> validNodes = new ArrayList<ComputationalNode>();
			ComputationalNode target = null;
			double minRuntime = Double.MAX_VALUE;
			double minCost = Double.MAX_VALUE;
			double maxBattery = Double.MIN_VALUE;
			
			for(ComputationalNode cn : compNodes)
			{
				if(!isValid(deployment,msc,cn))
					continue;
				else
				{
					double tmpRuntime = msc.getRuntimeOnNode(cn, I);
					double tmpCost = cn.computeCost(msc, I).getCost();
					double tmpBattery = I.getMobileDevice(msc.getUid()).getEnergyBudget() - 
							((cn.isMobile())? cn.getCPUEnergyModel().computeCPUEnergy(msc, cn, I) 
									: I.getMobileDevice(msc.getUid()).getNetEnergyModel().computeNETEnergy(msc, cn, I));

					if(tmpRuntime < minRuntime)
						minRuntime = tmpRuntime;
					if(tmpCost < minCost)
						minCost = tmpCost;
					if(tmpBattery > maxBattery)
						maxBattery = tmpBattery;
				}
			}
			double minScore = Double.MAX_VALUE;
			for(ComputationalNode n : compNodes){
				double tmpScore = computeScore(msc,n,I,minRuntime,minCost,maxBattery);
				if(minScore > tmpScore)
				{
					target = n;
					minScore = tmpScore;
				}
			}
			return target;
		//}
		//return null;

	}

	
}
