package at.ac.tuwien.ec.algorithms;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class HeuristicResearch extends Search {

	public HeuristicResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void findDeployments(Deployment deployment) {
		        
        //System.out.println(A.S);
        findCompatibleNodes();
        	
        Collections.sort(A.S, (Object o1, Object o2) -> {
            SoftwareComponent s1 = (SoftwareComponent) o1;
            SoftwareComponent s2 = (SoftwareComponent) o2;
            return Integer.compare(K.get(s1.getId()).size(), K.get(s2.getId()).size());             
        });
        
        search(deployment);
       
	}
	
	public Deployment search(Deployment deployment) {
        if (isComplete(deployment)) {
        	D.add((Deployment) deployment.clone());
            return deployment;
        }
        SoftwareComponent s = selectUndeployedComponent(deployment);
        MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
        if(!msc.isOffloadable())
        {
           	deploy(deployment,s,I.getMobileDevice());
           	Deployment d = search(deployment);
           	if(d != null)
           		return d;
        }
        if (K.get(s.getId()) != null) {
            for (ComputationalNode n : K.get(s.getId())) { // for all nodes compatible with s
                //System.out.println(steps + " Checking " + s.getId() + " onto node " + n.getId());
                if (isValid(deployment, s, n)) {
                    //System.out.println("Deploying " + s.getId() + " onto node " + n.getId());
                    deploy(deployment, s, n);
                    Deployment result = search(deployment);
                    if (result != null) {
                        return deployment;
                    }
                    else
                    undeploy(deployment, s, n);
                }
                //System.out.println("Undeploying " + s.getId() + " from node " + n.getId());
            }
        }
        return null;
    }

}
