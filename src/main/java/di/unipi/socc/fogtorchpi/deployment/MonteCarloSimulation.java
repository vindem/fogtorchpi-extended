package di.unipi.socc.fogtorchpi.deployment;

import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

import org.uma.jmetal.util.JMetalLogger;

import static java.util.Arrays.asList;

import java.util.ArrayList;

import at.ac.tuwien.ec.algorithms.WeightedFunctionResearch;
import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.Main;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.utils.DeploymentsHistogram;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import at.ac.tuwien.ec.utils.WorkloadSearch;

/**
 *
 * @author Stefano
 */
public class MonteCarloSimulation{

    private int times;
    private MobileCloudInfrastructure I;
    private ArrayList<MobileApplication> workload;
    private HashMap<String, HashSet<String>> businessPolicies;
    private int[] workloadRuns;

    public MonteCarloSimulation(
            int times,
            Application A,
            Infrastructure I) {

        this.times = times;
        this.workload = new ArrayList<MobileApplication>();
        workload.add((MobileApplication) A);
        this.I = (MobileCloudInfrastructure) I;
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
      
    public MonteCarloSimulation(
            int times,
            MobileApplication A,
            MobileCloudInfrastructure I) {

        this.times = times;
        this.workload = new ArrayList<MobileApplication>();
        workload.add((MobileApplication) A);
        this.I = I;
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
    
    public MonteCarloSimulation(
            int times,
            ArrayList<MobileApplication> workload,
            MobileCloudInfrastructure I) {
        this.times = times;
        this.workload = workload;
        this.I = new MobileCloudInfrastructure();
        businessPolicies = new HashMap<String, HashSet<String>>();
    }
    
    public void addBusinessPolicies(String component, List<String> allowedNodes) {
        HashSet<String> policies = new HashSet<>();
        policies.addAll(allowedNodes);
        this.businessPolicies.put(component, policies);
    }

    public HashMap<String,DeploymentsHistogram> startSimulation(List<String> fogNodes) {
    	//HashMap<Deployment, Couple<Double, Double>> histogram = new HashMap<>();
    	HashMap<String,DeploymentsHistogram> histograms = new HashMap<String,DeploymentsHistogram>();
    	//DeploymentsHistogram histogram = new DeploymentsHistogram();

    	for(int i = 0; i < SimulationConstants.algorithms.length; i++)
    		histograms.put(SimulationConstants.algorithms[i], new DeploymentsHistogram());
    	    	   	
    	ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();

    	for(String algorithm : histograms.keySet()){
    		ArrayList<Deployment> depList = new ArrayList<Deployment>();
    		List<Future<ArrayList<Deployment>>> futures = new LinkedList<Future<ArrayList<Deployment>>>();
    		for (int j = 0; j < times; j++) {
    			WorkloadSearch search = new WorkloadSearch();
    			MobileApplication A = extractWorkload(workload);
    			    			
    	    	I = Helper.setupInfrastructure();
    			I.sample();
    			A.sample();
    			
    			search.setCurrentApp(A.clone());
    			search.setCurrentInfrastructure(I.clone());
    			SimulationConstants.logger.info("Iteration "+ j);
    			//SimulationConstants.workload_runs = (int) Math.floor(ExponentialDistributionGenerator.getNext(SimulationConstants.workload_runs_lambda));
    			if(j%100 == 0)
    			{
    				//System.gc();
    				if(!SimulationConstants.batch)
    					System.out.println("Iteration " + j);
    			}

    			search.setChosenAlgorithm(algorithm);
    			//if(!algorithm.equals("nsgaIII"))
    			futures.add(executor.submit(search));
    			//ArrayList<Deployment> results = search.findDeployments();
				//if(results != null)
					//depList.addAll(results);
    		}


    		//if(!algorithm.equals("nsgaIII"))
    		{
    			for(int k = 0; k < futures.size(); k++)
    				try	{
    					ArrayList<Deployment> results = (ArrayList<Deployment>)futures.get(k).get();
    					if(results != null)
    						depList.addAll(results);
    				} catch (InterruptedException | ExecutionException e) {
    					// TODO Auto-generated catch block
    					e.printStackTrace();
    				} catch (NullPointerException | ArrayIndexOutOfBoundsException e1)
    			{
    					System.err.println("Error on " + algorithm);
    			} /*catch (TimeoutException e) {
    				// TODO Auto-generated catch block
    				System.err.println("Timeout expired on " + algorithm);
    			}*/
    			catch(Throwable t)
    			{
    				t.printStackTrace();
    			}
    		}
    		double pos = depList.size();
    		double size = depList.size();
    		System.out.println("Deployment found for " + algorithm + ": " + size);
    		for (Deployment d : depList) {
    			if (histograms.get(algorithm).containsKey(d)) {
    				//Double newCount = histogram.get(d).getA() + 1.0; //montecarlo frequency
    				//Double newPos = histogram.get(d).getB() + ((pos + 1)/ (size+1));
    				double scoreUpdate = ((pos + 1)/ (size+1));
    				histograms.get(algorithm).update(d, scoreUpdate);
    			} else {
    				//histogram.put(d, new Couple(1.0, pos / (size + 1)));
    				histograms.get(algorithm).add(d, pos / (size+1));
    			}
    			pos--;
    		}
    		//System.out.println(I);

    	}
    	System.out.println("Finished execution for: "
    			+ "\nAPP: " +SimulationConstants.targetApp
    			+ "\nALGORITHMS: " + SimulationConstants.algorithms);
    	return histograms;
    }

    public MobileApplication extractWorkload(ArrayList<MobileApplication> workload){
    	MobileWorkload mWorkload = new MobileWorkload(workload);
    	return mWorkload;
    }
	
}
