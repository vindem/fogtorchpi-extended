package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.SortedSet;


import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.algorithms.utils.NodeRankComparator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;

public class RandomDeploymentSearch extends DAGResearch{
		
	public RandomDeploymentSearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
		/*for(SoftwareComponent msc : A.S)
			System.out.println(msc.getId() + ":" + ((MobileSoftwareComponent)msc).getNodeRank() );
			*/
	}
	
	public ArrayList<Deployment> searchOnDAG(Deployment deployment){
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		PriorityQueue<MobileSoftwareComponent> tasks = 
				new PriorityQueue<MobileSoftwareComponent>(new NodeRankComparator(A, I));
		ArrayList<Deployment> deployments = new ArrayList<Deployment>();
		
		deployment = new Deployment();
		for(SoftwareComponent sc : A.S)
			tasks.add((MobileSoftwareComponent) sc);
		double currentRuntime = 0;
		MobileSoftwareComponent currTask = tasks.poll();
		while(!currTask.getId().equals("sink")){
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				currentRuntime = firstTaskToTerminate.getRuntime();
				deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
				scheduledNodes.remove(firstTaskToTerminate);
			}
			
			//System.out.print(currTask.getId() + " ");
			if(currTask.getId().equals("root"))
			{
				currTask = tasks.poll();
				continue;
			}
			ComputationalNode target = (currTask.isOffloadable())? getRandomTarget(I,currTask) : I.getMobileDevice(currTask.getUid());
			deployment.put(currTask, target);
			target.deploy(currTask);
			deployLinks(deployment,currTask,target);
			//deploy(deployment,currTask,target);
			//setRunningTime(deployment, (MobileSoftwareComponent)currTask, target);
			scheduledNodes.add(currTask);
			currTask = tasks.poll();
		}
		
		while(!scheduledNodes.isEmpty())
		{
			MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
			currentRuntime = firstTaskToTerminate.getRuntime();
			deployment.get(firstTaskToTerminate).getHardware().undeploy(firstTaskToTerminate.getHardwareRequirements());
			scheduledNodes.remove(firstTaskToTerminate);
		}
		//deployment.runTime += currentRuntime;
		SimulationConstants.logger.info("Deployment data: runtime=" + deployment.runTime 
				+ " battery lifetime: " + deployment.mobileEnergyBudget
				+ " cost: " + deployment.deploymentMonthlyCost.getCost());
		
		if(deployment != null && isComplete(deployment))
			deployments.add(deployment);
		return deployments;
	}

	private ComputationalNode getRandomTarget(MobileCloudInfrastructure i,SoftwareComponent cmp) {
		final int mobileChoice = 0;
		final int cloudChoice = 1;
		final int edgeChoice = 2;
		int infChoice = SimulationConstants.rand.nextInt((SimulationConstants.cloudOnly)? 2 : 3 );
		switch(infChoice)
		{
		case mobileChoice : return I.getMobileDevice(cmp.getUid());
		case cloudChoice : return (ComputationalNode) (i.C.values().toArray())[SimulationConstants.rand.nextInt(I.C.size())];
		case edgeChoice : return (ComputationalNode) (i.F.values().toArray())[SimulationConstants.rand.nextInt(I.F.size())];
		}
		return null;
	}

	@Override
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
