package at.ac.tuwien.ec.appmodel.mobileapps;

import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class FaceRecognizerApp extends MobileApplication{

	public FaceRecognizerApp() {
		super(0);
		sampleTasks();
		sampleLinks();
	}
	
	public FaceRecognizerApp(int id) {
		super(id);
		sampleTasks();
		sampleLinks();
	}
	
	public FaceRecognizerApp(int id, String mId) {
		super(id,mId);
		sampleTasks();
		sampleLinks();
	}
	
	@Override
	public MobileApplication clone() {
		FaceRecognizerApp cloned = new FaceRecognizerApp(this.workload_id);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		cloned.L = this.L;
		return cloned;
	}

	@Override
	public void sampleLinks() {
		addLink("FACERECOGNIZER_UI"+"_"+getWorkloadId()+","+mobileId,
				"FIND_MATCH"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("FIND_MATCH"+"_"+getWorkloadId()+","+mobileId,
				"FIND_MATCH_INIT"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("FIND_MATCH"+"_"+getWorkloadId()+","+mobileId,
				"DETECT_FACE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("FIND_MATCH_INIT"+"_"+getWorkloadId()+","+mobileId,
				"DETECT_FACE"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
		addLink("DETECT_FACE"+"_"+getWorkloadId()+","+mobileId,
				"FACERECOGNIZER_OUTPUT"+"_"+getWorkloadId()+","+mobileId,
				sampleLatency(),
				0.1);
	}

	@Override
	public void sampleTasks() {
		//double img_size = SimulationConstants.image_size + ExponentialDistributionGenerator.getNext(SimulationConstants.image_size); 
		double img_size = SimulationConstants.image_size;
		addComponent("FACERECOGNIZER_UI"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				//,5.0 + ExponentialDistributionGenerator.getNext(1.0/5.0)
        		,5.0e3*SimulationConstants.task_multiplier
				,5e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
		addComponent("FIND_MATCH_INIT"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				//,8.0 + ExponentialDistributionGenerator.getNext(1.0/8.0)
        		,8.0e3*SimulationConstants.task_multiplier
				,5e3*SimulationConstants.task_multiplier
        		,5e3*SimulationConstants.task_multiplier
        		);
		addComponent("FIND_MATCH"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				//,8.0 + ExponentialDistributionGenerator.getNext(1.0/8.0)
        		,8.0e3*SimulationConstants.task_multiplier
				,5e3*SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		);
		addComponent("DETECT_FACE"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				//,16.0 + ExponentialDistributionGenerator.getNext(1.0/16.0)
        		,16.0e3*SimulationConstants.task_multiplier
				,img_size*SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		);
		addComponent("FACERECOGNIZER_OUTPUT"+"_"+getWorkloadId()+","+mobileId,
				this.getMobileId(),
				asList("python"),
				new Hardware(1, 1, 1)
				,false
				//,8.0 + ExponentialDistributionGenerator.getNext(1.0/8.0)
        		,8.0e3*SimulationConstants.task_multiplier
				,img_size*SimulationConstants.task_multiplier
        		,img_size*SimulationConstants.task_multiplier
        		);		
	}

}
