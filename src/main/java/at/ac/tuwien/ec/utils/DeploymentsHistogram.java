package at.ac.tuwien.ec.utils;

import java.util.HashMap;
import java.util.List;

import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class DeploymentsHistogram extends HashMap<Deployment, DeploymentStatistics>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3605212001840784279L;
	private String algorithmName;
	
	public DeploymentsHistogram(){
		super();
	}
	
	public DeploymentsHistogram(String algorithmName){
		super();
		this.algorithmName = algorithmName;
	}
	
	public void add(Deployment d, double score)
	{
		DeploymentStatistics stats = new DeploymentStatistics();
		stats.setFrequency(1.0);
		stats.setScore(score);
		stats.addRuntime(d.runTime);
		stats.addBattery(d.mobileEnergyBudget);
		stats.addCost(d.deploymentMonthlyCost.getCost());
		stats.addProviderCost(d.providerCost);
		super.put(d,stats);
	}
	
	public void add(Deployment d, double freq, double score){
		DeploymentStatistics stats = new DeploymentStatistics();
		stats.setFrequency(freq);
		stats.setScore(score);
		stats.addRuntime(d.runTime);
		stats.addBattery(d.mobileEnergyBudget);
		stats.addCost(d.deploymentMonthlyCost.getCost());
		super.put(d, stats);
	}
	
	public void update(Deployment d,double score){
		if(super.containsKey(d))
		{
			DeploymentStatistics tmp = super.get(d);
			tmp.setFrequency(tmp.getFrequency() + 1.0);
			tmp.addBattery(d.mobileEnergyBudget);
			tmp.addRuntime(d.runTime);
			tmp.addCost(d.deploymentMonthlyCost.getCost());
			super.replace(d, tmp);
		}
	}

	public Double getFrequency(Deployment deployment) {
		return super.get(deployment).getFrequency();
	}
	
	public Double getScore(Deployment deployment){
		return super.get(deployment).getScore();
	}

	public double getAverageBattery(Deployment deployment) {
		Double tmp = new Double(0.0);
		for(Double d : super.get(deployment).getBattery())
			tmp += d;
		return (tmp.doubleValue() / super.get(deployment).getFrequency());
	}

	public double getAverageRuntime(Deployment deployment) {
		Double tmp = new Double(0.0);
		for(Double d: super.get(deployment).getRuntime())
			tmp += d;
		return (tmp.doubleValue() / super.get(deployment).getFrequency());
	}

	public double getAverageCost(Deployment deployment) {
		Double tmp = new Double(0.0);
		for(Double d : super.get(deployment).getCost())
			tmp += d;
		return (tmp.doubleValue() / super.get(deployment).getFrequency());
	}
	
	public double getAverageProviderCost(Deployment deployment) {
		Double tmp = new Double(0.0);
		for(Double d : super.get(deployment).getProviderCost())
			tmp += d;
		return (tmp.doubleValue() / super.get(deployment).getFrequency());
	}
	
	public double[] getRuntimeConfidenceInterval(Deployment deployment, double confidenceLevel){
		return super.get(deployment).getConfidenceInterval(super.get(deployment).getRuntime(), confidenceLevel);
	}
	
	public double[] getCostConfidenceInterval(Deployment deployment, double confidenceLevel){
		return super.get(deployment).getConfidenceInterval(super.get(deployment).getCost(), confidenceLevel);
	}
	
	public double[] getBatteryConfidenceInterval(Deployment deployment, double confidenceLevel){
		return super.get(deployment).getConfidenceInterval(super.get(deployment).getBattery(), confidenceLevel);
	}
	
}
