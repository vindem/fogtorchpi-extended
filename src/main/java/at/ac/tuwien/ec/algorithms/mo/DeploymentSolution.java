package at.ac.tuwien.ec.algorithms.mo;

import java.util.HashMap;

import org.uma.jmetal.solution.PermutationSolution;
import org.uma.jmetal.solution.Solution;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.Application;
import di.unipi.socc.fogtorchpi.application.ExactThing;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class DeploymentSolution implements PermutationSolution<ComputationalNode>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Deployment deployment;
	private HashMap<Object,Object> solutionAttributes;
	private MobileApplication A;
	private MobileCloudInfrastructure I;
	private double runTime = 0.0, cost = 0.0, battery = 0.0;
		
	public DeploymentSolution(Deployment deployment, MobileApplication A, MobileCloudInfrastructure I)
	{
		this.deployment = (Deployment) deployment.clone();
		this.A = A.clone();
		this.I = I.clone();
		solutionAttributes = new HashMap<Object,Object>();
		solutionAttributes.put("feasible", true);
	}
	
	@Override
	public Object getAttribute(Object arg0) {
		return solutionAttributes.get(arg0);
	}
	@Override
	public int getNumberOfObjectives() {
		return 3;
	}
	@Override
	public int getNumberOfVariables() {
		return deployment.size();
	}
	@Override
	public double getObjective(int arg0) {
		switch(arg0)
		{
		case 0: return runTime;
		case 1: return cost;
		case 2: return battery;
		default: return runTime;
		}
	}
	
	@Override
	public String getVariableValueString(int arg0) {
		return deployment.get(deployment.keySet().toArray()[arg0]).toString();
	}
	@Override
	public void setAttribute(Object arg0, Object arg1) {
		solutionAttributes.put(arg0, arg1);
	}
	
	@Override
	public void setObjective(int arg0, double arg1) {
		switch(arg0)
		{
		case 0: deployment.runTime = arg1;
				runTime = arg1;
				break;
		case 1: deployment.deploymentMonthlyCost.setCost(arg1);
				cost = arg1;
				break;
		case 2: deployment.mobileEnergyBudget = arg1;
				battery = SimulationConstants.batteryCapacity - arg1;
		}		
	}
	
	@Override
	public ComputationalNode getVariableValue(int arg0) {
		return deployment.get(deployment.keySet().toArray()[arg0]);
	}

	@Override
	public void setVariableValue(int arg0, ComputationalNode arg1) {
		MobileSoftwareComponent sc = (MobileSoftwareComponent) deployment.keySet().toArray()[arg0];
		ComputationalNode actual = deployment.get(sc);
		if(actual!=null)
			replace(deployment,sc,actual);
		else
			deploy(deployment,sc,arg1);
		//sc.setRuntime(sc.getRuntimeOnNode(arg1, I));
	}

	@Override
	public DeploymentSolution copy() {
		DeploymentSolution s = new DeploymentSolution(deployment,A,I);
		return s;
	}

	public Deployment getDeployment()
	{
		return deployment;
	}
	
	private void deploy(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        SimulationConstants.logger.info("Adding " + s.getId() + " = " + deployment.containsKey(s));
    	deployment.put(s, n);
        deployment.addCost(s,n, I);
        deployment.addEnergyConsumption(s, n, I);
        //deployment.addRuntime(s, n, I);
        //System.out.println(deployment + " " + deployment.size());
        n.deploy(s);
        deployLinks(deployment, s, n);
    }

    private void replace(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        if (deployment.containsKey(s)) {
        	n.undeploy(s);
        	//deployment.removeRuntime(s, n, I);
        	//deployment.removeCost(s, n, I);
            //deployment.removeEnergyConsumption(s, n, I);
        	undeployLinks(deployment, s, n);
        	deployment.replace(s,n);
        	//deployment.addCost(s,n, I);
            //deployment.addEnergyConsumption(s, n, I);
            n.deploy(s);
            deployLinks(deployment, s, n);
      }
       // System.out.println("UNDEP"+deployment);
    }
    
    private void deployLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        for (SoftwareComponent c : deployment.keySet()) {
            ComputationalNode m = deployment.get(c);
            Couple couple1 = new Couple(c.getId(), s.getId());
            Couple couple2 = new Couple(s.getId(), c.getId());

            if (A.L.containsKey(couple1) && A.L.containsKey(couple2)) {
                QoSProfile req1 = A.L.get(couple1); //c,s
                QoSProfile req2 = A.L.get(couple2); //s,c
                Couple c1 = new Couple(m.getId(), n.getId()); // m,n
                Couple c2 = new Couple(n.getId(), m.getId()); // n,m
                if (I.L.containsKey(c1)) {
                    QoSProfile pl1 = I.L.get(c1);
                    QoSProfile pl2 = I.L.get(c2);
                    pl1.setBandwidth(pl1.getBandwidth() - req1.getBandwidth());
                    pl2.setBandwidth(pl2.getBandwidth() - req2.getBandwidth());
                }
            }
        }

        for (ThingRequirement t : s.Theta) {
            ExactThing e = (ExactThing) t;
            if (n.isReachable(e.getId(), I, e.getQNodeThing(), e.getQThingNode())) {
                Couple c1 = new Couple(n.getId(), e.getId()); //c1 nodeThing

                QoSProfile pl1 = I.L.get(c1);
                QoSProfile pl2 = I.L.get(new Couple(e.getId(), n.getId()));

                pl1.setBandwidth(pl1.getBandwidth() - e.getQNodeThing().getBandwidth());
                pl2.setBandwidth(pl2.getBandwidth() - e.getQThingNode().getBandwidth());

            }
        }
    }
    
    private void undeployLinks(Deployment deployment, SoftwareComponent s, ComputationalNode n) {
        for (SoftwareComponent c : deployment.keySet()) {
            ComputationalNode m = deployment.get(c);
            Couple couple1 = new Couple(c.getId(), s.getId());
            Couple couple2 = new Couple(s.getId(), c.getId());

            if (A.L.containsKey(couple1) && A.L.containsKey(couple2)) {
                QoSProfile al1 = A.L.get(couple1);
                QoSProfile al2 = A.L.get(couple2);
                Couple c1 = new Couple(m.getId(), n.getId());
                Couple c2 = new Couple(n.getId(), m.getId());
                if (I.L.containsKey(c1)) {
                    QoSProfile pl1 = I.L.get(c1);
                    QoSProfile pl2 = I.L.get(c2);

                    pl1.setBandwidth(pl1.getBandwidth() + al1.getBandwidth());
                    pl2.setBandwidth(pl2.getBandwidth() + al2.getBandwidth());
                }
            }

        }

        for (ThingRequirement t : s.Theta) {
            ExactThing e = (ExactThing) t;
            //System.out.println("Request" + e);
            if (n.isReachable(e.getId(), I, e.getQNodeThing(), e.getQThingNode())) {
                Couple c1 = new Couple(n.getId(), e.getId());

                QoSProfile pl1 = I.L.get(c1);
                QoSProfile pl2 = I.L.get(new Couple(e.getId(), n.getId()));

                pl1.setBandwidth(pl1.getBandwidth() + e.getQNodeThing().getBandwidth());
                pl2.setBandwidth(pl2.getBandwidth() + e.getQThingNode().getBandwidth());

            }
        }
    }

	public MobileApplication getApplication() {
		return A;
	}

	public MobileCloudInfrastructure getInfrastructure() {
		return I;
	}
	
	public Number getLowerBound(int index) {
		return 0.0;
	}
		
}
