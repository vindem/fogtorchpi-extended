package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;

public class FirstFitResearch extends Search {
		
	private int found = 0;

	public FirstFitResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void findDeployments(Deployment deployment) {
		        
        //System.out.println(A.S);
        findCompatibleNodes();
        	
        Collections.sort(A.S, (Object o1, Object o2) -> {
            SoftwareComponent s1 = (SoftwareComponent) o1;
            SoftwareComponent s2 = (SoftwareComponent) o2;
            return Integer.compare(K.get(s1.getId()).size(), K.get(s2.getId()).size());             
        });
        
        //deployment = search();
        searchOnDAG(deployment);
        resetSearch(deployment);
    }
	
	public Deployment searchOnDAG(Deployment deployment) {
		
		while(!isComplete(deployment)){
			ArrayList<MobileSoftwareComponent> eligibleNodes = selectElegibleNodes();
	        
			for(SoftwareComponent s:eligibleNodes){
	        	MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
	        	if(!msc.isOffloadable())
	        	{
	        		if(isValid(deployment,msc,I.getMobileDevice()))
	        		{
	        			deploy(deployment,msc,I.getMobileDevice());
	        			double currRuntime = 0;
	        			for(MobileSoftwareComponent sc : A.getPredecessors(msc))
	        			{
	        				String id1 = deployment.get(sc).getId();
	        				String id2 = deployment.get(msc).getId();
	        				double tmpRuntime = sc.getRuntime() +  I.getTransmissionTime(msc,id1,id2);
	        				if(tmpRuntime > currRuntime)
	        					currRuntime = tmpRuntime;
	        			}
	        			msc.setRuntime((int) (currRuntime + msc.getRuntimeOnNode(I.getMobileDevice(), I)));
	        			if(msc.getRuntime() > deployment.runTime)
	        				deployment.runTime = msc.getRuntime();
	        		}
	        		else
	        		{
	        			resetSearch(deployment);
	        			return null;
	        		}
	        	}
	        	else
	        	{
	        		if (K.get(s.getId()) != null) 
	        		{
	        			ArrayList<ComputationalNode> compNodes = K.get(s.getId());
	        			int i;
	        			for(i = 0; i < compNodes.size(); i++)
	        			{
	        				ComputationalNode cn = compNodes.get(i);
	        				if(!isValid(deployment,s,cn))
	        					continue;
	        				else
	        				{
	        					deploy(deployment,s,cn);
	        					double currRuntime = 0;
	    	        			for(MobileSoftwareComponent sc : A.getPredecessors(msc))
	    	        			{
	    	        				String id1 = deployment.get(sc).getId();
	    	        				String id2 = deployment.get(msc).getId();
	    	        				double tmpRuntime = sc.getRuntime() +  I.getTransmissionTime(sc,id1,id2);
	    	        				if(tmpRuntime > currRuntime)
	    	        					currRuntime = tmpRuntime;
	    	        			}
	    	        			msc.setRuntime((int) (currRuntime + msc.getRuntimeOnNode(cn, I)));
	    	        			if(msc.getRuntime() > deployment.runTime)
	    	        				deployment.runTime = msc.getRuntime();
	        					break;
	        				}
	        			}
	        			if(i == compNodes.size())
	        			{
	        				resetSearch(deployment);
	        				return null;
	        			}
	        		}
	        		else{
	        			resetSearch(deployment);
	        			return null; 
	        		}
	        	}
	        }
		}
		Deployment tmp = (Deployment) deployment.clone();
		D.add(tmp);
		return deployment;
	}
	
	public Deployment search() {
		Deployment deployment = new Deployment();
		while(!isComplete(deployment)){
			SoftwareComponent s = selectUndeployedComponent(deployment);
	        MobileSoftwareComponent msc = (MobileSoftwareComponent) s;
	        if(!msc.isOffloadable())
	        {
	        	if(isValid(deployment,msc,I.getMobileDevice()))
	        		deploy(deployment,msc,I.getMobileDevice());
	        	else
	        	{
	        		resetSearch(deployment);
	        		return null;
	        	}
	        }
	        else
	        {
	        	if (K.get(s.getId()) != null) 
	        	{
	                ArrayList<ComputationalNode> compNodes = K.get(s.getId());
	                int i;
	                for(i = 0; i < compNodes.size(); i++)
	                {
	                	ComputationalNode cn = compNodes.get(i);
	                	if(!isValid(deployment,s,cn))
	                		continue;
	                	else
	                	{
	                		deploy(deployment,s,cn);
	                		break;
	                	}
	                }
	                if(i == compNodes.size())
	                {
	                	resetSearch(deployment);
	                	return null;
	                }
	            }
	            else{
	            	resetSearch(deployment);
	            	return null; 
	            }
	         }
		}
		Deployment tmp = (Deployment) deployment.clone();
		D.add(tmp);
		return deployment;
	}
	
}
