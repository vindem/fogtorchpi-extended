package at.ac.tuwien.ec.infrastructuremodel.energy;

import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;

public class SamsungS2DualEnergyModel implements CPUEnergyModel {

	private final double BETA_1CORE = 6.93205;
	private final double BETA_2CORE = 11.248;
	//PLEASE REMEMBER: THOSE ARE MICROWATT!!!!
	private final double BETAFREQBASE_1CORE = 625.25e-6;
	private final double BETAFREQBASE_2CORE = 597.79e-6;
	@Override
	public double computeCPUEnergy(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		//In this version, we assume that mobile is at maximum frequency per core, 1200MHz
		double utilization = s.getHardwareRequirements().cores / n.getHardware().maxCores;
		double power = BETA_1CORE * utilization + BETAFREQBASE_1CORE;
		double runtime = s.getRuntimeOnNode(n, i);
		return power * runtime;
	}
	@Override
	public double getIdlePower(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		// TODO Auto-generated method stub
		return 0;
	}

}
