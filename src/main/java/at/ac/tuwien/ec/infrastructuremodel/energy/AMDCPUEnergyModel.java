package at.ac.tuwien.ec.infrastructuremodel.energy;

import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;

public class AMDCPUEnergyModel implements CPUEnergyModel {

	final double LOAD_LEVEL = 0.12;
	final double ALPHA = 5.29;
	final double BETA = 0.68;
	final double Pidle = 501;
	final double Pmax = 840;
	final double Pr = Pmax - Pidle;
	
	@Override
	public double computeCPUEnergy(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		double load = (double) s.getHardwareRequirements().cores / (double) n.getHardware().cores;
		double power = ((load <= LOAD_LEVEL)? ALPHA * Pr * load : 
			BETA * Pr + (1-BETA) * Pr) + Pidle;
		double runtime = s.getRuntimeOnNode(n, i);
		return power*s.getHardwareRequirements().cores*runtime 
				+ Pidle * (n.getHardware().cores - s.getHardwareRequirements().cores) * runtime;
	}

	@Override
	public double getIdlePower(SoftwareComponent s, ComputationalNode n, MobileCloudInfrastructure i) {
		return s.getRuntimeOnNode(n, i) * n.getHardware().cores * Pidle;
	}

}
