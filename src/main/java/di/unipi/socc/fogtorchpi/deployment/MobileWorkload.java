package di.unipi.socc.fogtorchpi.deployment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.appmodel.mobileapps.ChessApp;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

public class MobileWorkload extends MobileApplication {

	private ArrayList<MobileApplication> workload;
	
	
	public MobileWorkload(ArrayList<MobileApplication> workload)
	{
		this.workload = new ArrayList<MobileApplication>();
		this.workload.addAll(workload);
	}
	
	@Override
	public MobileApplication clone() {
		MobileWorkload cloned = new MobileWorkload(this.workload);
		cloned.S.clear();
		cloned.L.clear();
		cloned.S = (ArrayList<SoftwareComponent>) this.S.clone();
		//cloned.L = (ConcurrentHashMap<Couple, QoSProfile>) this.L.clone();
		cloned.L = (LinkedHashMap<Couple<String, String>, QoSProfile>) this.L.clone();
		return cloned;
	}

	@Override
	public void sampleLinks() {
		this.L.clear();
		for(int i = 0; i < workload.size() - 1; i++)
		{
			MobileApplication currApp = workload.get(i);
			currApp.sampleLinks();
			for(Couple<String,String> c : currApp.L.keySet())
				this.L.put(c, currApp.L.get(c));
			int lastIdx = currApp.S.size()-1;
			MobileApplication nextApp = workload.get(i+1);
			String id2 = nextApp.S.get(0).getId();
			for(int j = 0; j < currApp.S.size(); j++)
			{
				SoftwareComponent cmp = currApp.S.get(j);
				if(currApp.getNeighbors(cmp).isEmpty())
				{
					String id1 = cmp.getId();
					addLink(id1,id2,Integer.MAX_VALUE,0.0);
				}
			}
		}
		workload.get(workload.size()-1).sampleLinks();
		for(Couple<String,String> c : workload.get(workload.size()-1).L.keySet())
			this.L.put(c, workload.get(workload.size()-1).L.get(c));
		
	}

	@Override
	public void sampleTasks() {
		this.S.clear();
		for(int i = 0; i < workload.size(); i++){
			MobileApplication app = workload.get(i);
			this.S.addAll(app.S);
		}
	}

	public void sample() {
		S.clear();
		L.clear();
		sampleTasks();
		sampleLinks();
	}
	
}
