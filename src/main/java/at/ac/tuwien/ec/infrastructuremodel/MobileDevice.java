package at.ac.tuwien.ec.infrastructuremodel;

import java.util.Collection;
import java.util.HashSet;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.utils.Constants;
import di.unipi.socc.fogtorchpi.utils.Coordinates;
import di.unipi.socc.fogtorchpi.utils.Cost;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.Software;

public class MobileDevice extends ComputationalNode {

	private double energyBudget = 0.0;
	
	public MobileDevice()
	{
		
	}
	
	public MobileDevice(String identifier, Collection<Couple<String, Double>> software, Hardware hw, double x, double y){
		super.setId(identifier);
	    super.setHardware(hw);
	    super.setSoftware(software);
	    super.setCoordinates(x,y);
	    super.setKeepLight(false);
	    isMobile = true;
	}
	
	public MobileDevice(String identifier, Collection<Couple<String, Double>> software, Hardware hw, double x, double y, double energyBudget){
		super.setId(identifier);
	    super.setHardware(hw);
	    super.setSoftware(software);
	    super.setCoordinates(x,y);
	    super.setKeepLight(false);
	    isMobile = true;
	    this.energyBudget = energyBudget;
	}
	
	public boolean isCompatible(MobileSoftwareComponent s, MobileCloudInfrastructure I) {
		Hardware hardwareRequest = s.getHardwareRequirements();
        Collection<Software> softwareRequest = s.getSoftwareRequirements();
        
        if(!s.getUid().equals(this.getId()))
        	return false;
        if(super.getHardware().supports(hardwareRequest) && 
                softwareRequest.stream().noneMatch(
                        (t) -> (!super.getSoftware().containsValue(t))
                ))
        	return getCPUEnergyModel().computeCPUEnergy(s, this, I) < this.energyBudget;
        return false;
	}

	@Override
	public void deploy(SoftwareComponent s) {
		super.getHardware().deploy(s.getHardwareRequirements());		
	}

	@Override
	public void undeploy(SoftwareComponent s) {
		super.getHardware().undeploy(s.getHardwareRequirements());
	}

	@Override
	public Cost computeCost(SoftwareComponent s, Infrastructure I) {
		// TODO Auto-generated method stub
		return new Cost(0.0);
	}

	@Override
	public double computeHeuristic(SoftwareComponent s) {
		this.heuristic = super.getHardware().cores/Constants.MAX_CORES + 
                super.getHardware().ram/Constants.MAX_RAM + 
                super.getHardware().storage/Constants.MAX_HDD; //+ 1/(deploymentLocation.distance(this.getCoordinates()));
        
        if (this.getKeepLight()){
            heuristic = heuristic - 4;
        }

        return heuristic;
	}

	public double getEnergyBudget() {
		return energyBudget;
	}

	public void setEnergyBudget(double energyBudget) {
		this.energyBudget = energyBudget;
	}

	public void removeFromBudget(double computeCPUEnergyConsumption) {
		this.energyBudget -= computeCPUEnergyConsumption;
	}

	public void addToBudget(double computeCPUEnergyConsumption) {
		this.energyBudget += computeCPUEnergyConsumption;
		
	}
	
	public String toString(){
		return this.getHardware().toString();
	}

	public MobileDevice clone(){
		MobileDevice dev = new MobileDevice();
		dev.setId(this.getId());
	    dev.setHardware(this.getHardware());
	    dev.setSoftware(this.getSoftware());
	    dev.setCoordinates(this.getCoordinates());
	    dev.setKeepLight(false);
	    dev.setCPUEnergyModel(getCPUEnergyModel());
	    dev.setNetEnergyModel(getNetEnergyModel());
	    isMobile = true;
	    dev.setEnergyBudget(this.energyBudget);
	   	return dev;
	}

	@Override
	public boolean isCompatible(SoftwareComponent s) {
		Hardware hardwareRequest = s.getHardwareRequirements();
        Collection<Software> softwareRequest = s.getSoftwareRequirements();
        
        
        if(super.getHardware().supports(hardwareRequest) && 
                softwareRequest.stream().noneMatch(
                        (t) -> (!super.getSoftware().containsValue(t))
                ))
        	return getCPUEnergyModel().computeCPUEnergy(s, this, null) < this.energyBudget;
        return false;
	}

	@Override
	public void sampleNode() {
		int x = SimulationConstants.rand.nextInt(SimulationConstants.MAP_M);
		int y = SimulationConstants.rand.nextInt(SimulationConstants.MAP_N);
		this.setCoordinates(x, y);
	}
	
}
