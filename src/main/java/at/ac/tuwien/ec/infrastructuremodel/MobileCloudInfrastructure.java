/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.ac.tuwien.ec.infrastructuremodel;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.infrastructure.CloudDatacentre;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.infrastructure.FogNode;
import di.unipi.socc.fogtorchpi.infrastructure.Infrastructure;
import di.unipi.socc.fogtorchpi.infrastructure.Thing;
import di.unipi.socc.fogtorchpi.utils.Coordinates;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.QoS;
import di.unipi.socc.fogtorchpi.utils.QoSProfile;

import static java.util.Arrays.asList;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.Helper;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants.Timezone;
import at.ac.tuwien.ec.infrastructuremodel.energy.AMDCPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.Mobile3GNETEnergyModel;
import at.ac.tuwien.ec.infrastructuremodel.energy.MobileWiFiNETEnergyModel;
import at.ac.tuwien.ec.utils.ExponentialDistributionGenerator;

/**
 *
 * @author Vincenzo
 */
public class MobileCloudInfrastructure extends Infrastructure {
    
	private static final double SECONDS_PER_HOUR = 3600;
	private static final double maxHops = 12;
	public HashMap<String,MobileDevice> M;
	
	public MobileCloudInfrastructure()
	{
		super();
		M = new HashMap<String,MobileDevice>();
	}
	
	public String getVMList(){
		String s = "";
		for(CloudDatacentre c: C.values())
			s += c.getVirtualMachines();
		return s;
	}
	
	public void addMobileDevice(MobileDevice device)
	{
		M.put(device.getId(), device);
		L.put(new Couple<String,String>(device.getId(),device.getId()), new QoSProfile(0, Double.MAX_VALUE));
	}
	
	public MobileDevice getMobileDevice(String id){
		return M.get(id);
	}
	
	public Couple<Double, Double> consumedResources(Deployment d){
        
        Couple<Double, Double> consumedResources = new Couple(0.0,0.0);
        Couple<Double, Double> allResources = new Couple(0.0,0.0);
        
        for (FogNode g : F.values()){
                allResources.setA(g.getHardware().ram + allResources.getA());
                allResources.setB(g.getHardware().storage + allResources.getB());
                
        }
        
        for (SoftwareComponent s: d.keySet()){
            if (F.containsKey(d.get(s).getId())){
            	
                FogNode f = (FogNode) d.get(s); 
                Couple<Double, Double> tmp = f.consumedResources(s);
                
                consumedResources.setA(tmp.getA() + consumedResources.getA());
                consumedResources.setB(tmp.getB() + consumedResources.getB());
            }
        }
        
        return new Couple<Double,Double>(consumedResources.getA()/allResources.getA(),
                consumedResources.getB()/allResources.getB());
    }

	public void addFogNode(String identifier, List<Couple<String, Double>> software, Hardware h, double x, double y,
			List<Couple<String, Double>> vmTypes) {
		F.put(identifier,new FogNode(identifier, software, x, y, h, vmTypes));
        L.put(new Couple(identifier,identifier), new QoSProfile(0, Double.MAX_VALUE));
		
	}

	public void addCloudDatacentre(String string, List<Couple<String, Double>> asList, double d, double e,
			Hardware hardware, CPUEnergyModel cpuEnergyModel, double i) {
		C.put(string,new CloudDatacentre(string,asList,d,e,hardware,null,cpuEnergyModel,i));
		L.put(new Couple(string,string), new QoSProfile(0, Double.MAX_VALUE));
	}
	
	public void addFogNode(String identifier, List<Couple<String, Double>> software, Hardware h, double x, double y,
			CPUEnergyModel cpuEnergyModel, double i) {
		F.put(identifier,new FogNode(identifier, software, x, y, h,null, cpuEnergyModel, i));
        L.put(new Couple(identifier,identifier), new QoSProfile(0, Double.MAX_VALUE));
		
	}

	public double getTransmissionTime(MobileSoftwareComponent s,String id1, String id2) {
		QoSProfile profile = L.get(new Couple<String,String>(id1,id2));
		if(profile == null)
			profile = L.get(new Couple<String,String>(id2,id1));
		if(profile == null)
			return Double.MAX_VALUE;
		
		if(profile.getLatency()==Integer.MAX_VALUE)
			return Double.MAX_VALUE;
		//return s.getOffloadData()/(profile.getBandwidth()*8e+6) + 
		//		((s.getOffloadData()/65535)*(profile.getLatency()/1000));
		return (((s.getOutData() + s.getOffloadData())/(profile.getBandwidth()*125000.0) + 
				((profile.getLatency()*computeDistance(id1,id2))/1000.0)) ); //*SimulationConstants.offloadable_part_repetitions;
	}
	
	public double getTransmissionTimeWithQoS(MobileSoftwareComponent s,String id1, String id2, QoSProfile profile) {
		if(profile.getLatency()==Integer.MAX_VALUE)
			return Double.MAX_VALUE;
		//return s.getOffloadData()/(profile.getBandwidth()*8e+6) + 
		//		((s.getOffloadData()/65535)*(profile.getLatency()/1000));
		return (((s.getOutData() + s.getOffloadData())/(profile.getBandwidth()*125000.0) + 
				((profile.getLatency()*computeDistance(id1,id2))/1000.0)) ); //*SimulationConstants.offloadable_part_repetitions;
	}
	
	private double computeDistance(String id1, String id2) {
		Coordinates c1,c2;
		if(isCloudNode(id1) || isCloudNode(id2))
			return maxHops;
		if(id1.equals(id2))
			return 0.0;
		if(isMobileNode(id1))
		{
			if(F.get(id2)==null)
				System.out.println(id2);
			c1 = M.get(id1).getCoordinates();
			c2 = F.get(id2).getCoordinates();
			
		}
		else
		{
			c1 = F.get(id1).getCoordinates();
			c2 = M.get(id2).getCoordinates();
		}
		return (Math.abs(c1.getLatitude()-c2.getLatitude()) 
				+ Math.max(0, 
						(Math.abs(c1.getLatitude()-c2.getLatitude())
								- Math.abs(c1.getLongitude()-c2.getLongitude()) )/2));
		
	}

	public boolean isCloudNode(String id){
		return C.containsKey(id);
	}
	
	public boolean isFogNode(String id){
		return F.containsKey(id);
	}
	
	public boolean isMobileNode(String id){
		return M.containsKey(id);
	}

	public void sample() {
		for(QoSProfile q: L.values())
			q.sampleQoS();
		for(CloudDatacentre c: C.values())
			c.sampleNode();
		for(FogNode f: F.values())
			f.sampleNode();
		for(MobileDevice m: M.values())
			m.sampleNode();
	}

	public MobileCloudInfrastructure clone()
	{
		MobileCloudInfrastructure cloned = new MobileCloudInfrastructure();
		cloned.C = (HashMap<String, CloudDatacentre>) this.C.clone();
		cloned.F = (HashMap<String, FogNode>) this.F.clone();
		cloned.L = (HashMap<Couple, QoSProfile>) this.L.clone();
		cloned.T = (HashMap<String, Thing>) this.T.clone();
		cloned.M = (HashMap<String,MobileDevice>) this.M.clone();
		cloned.prices = (HashMap<Coordinates,Double[]>) this.prices.clone();
		return cloned;
	}
	
	public String toString(){
		String tmp = "";
		tmp += "CLOUD NODES:\n";
		tmp += C + " ;\n";
		tmp += "EDGE NODES:\n";		
		tmp += F + " ;\n";
		tmp += "MOBILE DEVICES:\n";
		tmp += M + ".\n";
		return tmp;
	}

	public void clearAll() {
		C.clear();
		F.clear();
		T.clear();
		L.clear();
		M.clear();
	}

	//price per joule/hour
	public double getPriceForLocation(Coordinates coordinates, double time) {
		//return 32.79;
		
		for(Coordinates crd : prices.keySet())
			if(crd.equals(coordinates))
			{
				Double[] currPrices = prices.get(crd);
				int index = (int) Math.floor(time / SECONDS_PER_HOUR);
				Timezone tz = getTimezoneFromCoordinates(crd);
				int tOffset = tz.getTZOffset();
				index = (index+tOffset)<0? currPrices.length - (tOffset+index) 
						: (index+tOffset)%currPrices.length;
				return (currPrices[index] * 3.60e-9);
			}
		return Double.MAX_VALUE;
		
	}

	private Timezone getTimezoneFromCoordinates(Coordinates crd) {
		for(int i = 0; i < Helper.timezones.length; i++)
			if(crd.getLatitude() == Helper.timezones[i].getX()
			&& crd.getLatitude() == Helper.timezones[i].getY())
				return Helper.timezones[i];
		return SimulationConstants.Timezone.DUBLIN;
	}

	public void addPrices(Coordinates coordinates, Double[] d) {
		prices.put(coordinates, d);
	}
	
	public void addLink(String a, String b, int latency, double bandwidth) {
        if(M.containsKey(a) && M.containsKey(b) && !a.equals(b))
           	return;
        L.put(new Couple<String,String>(a,b), new QoSProfile(latency,bandwidth));
        L.put(new Couple<String,String>(b,a), new QoSProfile(latency,bandwidth));
    }

    public void addLink(String a, String b, int latency, double bandwidthba, double bandwidthab) {
    	if(M.containsKey(a) && M.containsKey(b) && !a.equals(b))
           	return;
        L.put(new Couple<String,String>(a,b), new QoSProfile(latency,bandwidthab));
        L.put(new Couple<String,String>(b,a), new QoSProfile(latency,bandwidthba));
    }
    
    public void addLink(String a, String b, QoSProfile q) {
    	if(M.containsKey(a) && M.containsKey(b)  && !a.equals(b))
           	return;
        L.put(new Couple<String,String>(a,b), q);
        L.put(new Couple<String,String>(b,a), new QoSProfile(q.getLatency(), q.getBandwidth()));
    }

    public void addLink(String a, String b, QoSProfile downlinkba, QoSProfile uplinkab) { //q1 dwn uplinkab upl
    	if(M.containsKey(a) && M.containsKey(b)  && !a.equals(b))
          	return;
       	L.put(new Couple<String,String>(b,a), downlinkba);
        L.put(new Couple<String,String>(a,b), uplinkab);
    }
    	
	private HashMap<String, Coordinates> createEdgePlan(String edgePlanningAlgorithm, boolean[][] edgeMap) {
		if(F!=null)
			F.clear();
		HashMap<String,Coordinates> edgePlan = new HashMap<String,Coordinates>(); 
		switch(edgePlanningAlgorithm)
		{
			case "mapbased":
				if(edgeMap!=null){
					for(int i = 0; i < SimulationConstants.MAP_M; i++)
						for(int j = 0; j < SimulationConstants.MAP_N; j++)
						{
							if(edgeMap[i][j])
								edgePlan.put("edge_"+i+","+j, new Coordinates(i,j));
						}
				}
			case "random":
				for(int i = 0; i < SimulationConstants.MAP_M; i++)
					for(int j = 0; j < SimulationConstants.MAP_N; j++)
					{
						if(SimulationConstants.rand.nextBoolean())
						{
							if(i % 2 == 0 && j%2 == 0)
								edgePlan.put("edge_"+i+","+j, new Coordinates(i,j));
							if(i%2==1 && j%2==1)
								edgePlan.put("edge_"+i+","+j, new Coordinates(i,j));
						}
					}
				break;
			case "allcell":
				for(int i = 0; i < SimulationConstants.MAP_M; i++)
					for(int j = 0; j < SimulationConstants.MAP_N; j++)
					{
						if(i % 2 == 0 && j%2 == 0)
							edgePlan.put("edge_"+i+","+j, new Coordinates(i,j));
						if(i%2==1 && j%2==1)
							edgePlan.put("edge_"+i+","+j, new Coordinates(i,j));
					}						
				break;			
			default: edgePlan.put("edge_0",new Coordinates(3.0,1.0));
					edgePlan.put("edge_1", new Coordinates(9.0,1.0));
					edgePlan.put("edge_2", new Coordinates(2.0,4.0));
					edgePlan.put("edge_3", new Coordinates(8.0,4.0));
		}
		return edgePlan;
	}
	

	public void setupEdgeNodes(int edgeCoreNum, HashMap<Timezone, Double[]> timezoneData, String edgePlanningAlgorithm, boolean wifi) {
		Couple[] links = new Couple[L.keySet().size()];
		L.keySet().toArray(links);
		for(int i = 0; i < links.length; i++)
			if(F.containsKey(links[i].getA()) || F.containsKey(links[i].getB()))
				L.remove(links[i]);
		if(F!=null)
			F.clear();
		HashMap<String,Coordinates> edgePlan = createEdgePlan(edgePlanningAlgorithm,null);
		for(String edgeNodeId : edgePlan.keySet())
    	{
    		addFogNode(edgeNodeId, asList(new Couple<String,Double>("python",0.0),
        			new Couple<String,Double>("c++",0.0),
        			new Couple<String,Double>("mySQL",0.0),
        			new Couple<String,Double>(".NETcore",0.0),
        			new Couple<String,Double>("linux",0.0)),
        			//new Hardware((int) Math.floor(SimulationConstants.rand.nextGaussian() * SimulationConstants.core_variance + 8), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        			new Hardware(edgeCoreNum, 128, (int)10e12, 0.03, 0.02, 0.01),
        			edgePlan.get(edgeNodeId).getLatitude(),
        			edgePlan.get(edgeNodeId).getLongitude(),
        			//new Hardware((int) Math.floor(ExponentialDistributionGenerator.getNext(1.0/16.0)), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        			new AMDCPUEnergyModel(),
        			2000);
    		addPrices(new Coordinates(edgePlan.get(edgeNodeId).getLatitude()
    				,edgePlan.get(edgeNodeId).getLongitude())
    				,timezoneData.get(SimulationConstants.Timezone.DUBLIN));
    	}
		
		String[] edgeNodeIds = new String[F.keySet().size()];
		F.keySet().toArray(edgeNodeIds);
		
		for(String mId: M.keySet())
		{
			double firstHop3GBandwidth = ExponentialDistributionGenerator.getNext(1.0/7.2);
			double firstHopWiFiHQBandwidth = ExponentialDistributionGenerator.getNext(1.0/32.0);
			double firstHopWiFiLQBandwidth = ExponentialDistributionGenerator.getNext(1.0/4.0);

			QoSProfile qos3gUP = new QoSProfile(asList(
					new Couple<QoS,Double>(new QoS(54.0, firstHop3GBandwidth), 0.9957),
					new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0043)));
			QoSProfile qos3gDN = new QoSProfile(asList(
					new Couple<QoS,Double>(new QoS(54.0, firstHop3GBandwidth), 0.9959),
					new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0.0), 0.0041)));
			QoSProfile qosWifiUP = new QoSProfile(asList(
					new Couple<QoS,Double>(new QoS(15.0, firstHopWiFiHQBandwidth), 0.9),
					new Couple<QoS,Double>(new QoS(15.0, firstHopWiFiLQBandwidth), 0.09),
					new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
					));
			QoSProfile qosWifiDN = new QoSProfile(asList(
					new Couple<QoS,Double>(new QoS(15.0, firstHopWiFiHQBandwidth), 0.9),
					new Couple<QoS,Double>(new QoS(15.0, firstHopWiFiLQBandwidth), 0.09),
					new Couple<QoS,Double>(new QoS(Double.MAX_VALUE, 0), 0.01)
					));

			QoSProfile qosUpFog,qosDownFog,qosDownCloud,qosUpCloud;
			
			getMobileDevice(mId).setNetEnergyModel((wifi)? new MobileWiFiNETEnergyModel() : new Mobile3GNETEnergyModel());
			qosUpFog = (wifi)? qosWifiUP : qos3gUP;
			qosDownFog = (wifi)? qosWifiDN : qos3gDN;

			for(int i = 0; i < edgeNodeIds.length; i++)
			{
				addLink(mId, edgeNodeIds[i],qosUpFog,qosDownFog);
        		addLink(edgeNodeIds[i], mId,qosUpFog,qosDownFog);
        	}
		}
	}
	
	public void setupEdgeNodes(int edgeCoreNum, HashMap<Timezone, Double[]> timezoneData, boolean[][] map) {
		F.clear();
		HashMap<String,Coordinates> edgePlan = createEdgePlan("mapbased",map);
		for(String edgeNodeId : edgePlan.keySet())
    	{
    		addFogNode(edgeNodeId, asList(new Couple<String,Double>("python",0.0),
        			new Couple<String,Double>("c++",0.0),
        			new Couple<String,Double>("mySQL",0.0),
        			new Couple<String,Double>(".NETcore",0.0),
        			new Couple<String,Double>("linux",0.0)),
        			//new Hardware((int) Math.floor(SimulationConstants.rand.nextGaussian() * SimulationConstants.core_variance + 8), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        			new Hardware(edgeCoreNum, 128, (int)10e12, 0.03, 0.02, 0.01),
        			edgePlan.get(edgeNodeId).getLatitude(),
        			edgePlan.get(edgeNodeId).getLongitude(),
        			//new Hardware((int) Math.floor(ExponentialDistributionGenerator.getNext(1.0/16.0)), 128, 1000, 0.03, 0.02, 0.01), 43.740186, 10.364619,
        			new AMDCPUEnergyModel(),
        			2000);
    		addPrices(new Coordinates(edgePlan.get(edgeNodeId).getLatitude()
    				,edgePlan.get(edgeNodeId).getLongitude())
    				,timezoneData.get(SimulationConstants.Timezone.DUBLIN));
    	}
		
	}
	
}
