package at.ac.tuwien.ec.algorithms.utils;

import java.util.Comparator;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;

public class NodeRankComparator implements Comparator<MobileSoftwareComponent>
{

	private MobileApplication A;
	private MobileCloudInfrastructure I;
	
	public NodeRankComparator(MobileApplication A, MobileCloudInfrastructure I)
	{
		this.A = A;
		this.I = I;
	}
	@Override
	public int compare(MobileSoftwareComponent o1, MobileSoftwareComponent o2) {
		if( o1.getNodeRank() > o2.getNodeRank() )
			return -1;
		else if( o1.getNodeRank() < o2.getNodeRank() )
			return 1;
		else
			return 0;
	}
		
}
