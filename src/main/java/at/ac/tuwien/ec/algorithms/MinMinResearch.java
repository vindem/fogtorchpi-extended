package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class MinMinResearch extends DAGResearch {
	
	public MinMinResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public MinMinResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	/*public Deployment searchOnDAG(Deployment deployment) {
		ArrayList<MobileSoftwareComponent> scheduledNodes 
		= new ArrayList<MobileSoftwareComponent>();
		HashMap<ComputationalNode,MobileSoftwareComponent> partialDeploy 
		= new HashMap<ComputationalNode,MobileSoftwareComponent>();
		double currentRuntime = 0;
		while(!isComplete(deployment)){
			
			if(!scheduledNodes.isEmpty())
			{
				MobileSoftwareComponent firstTaskToTerminate = getFirstToTerminate(scheduledNodes);
				SimulationConstants.logger.info(firstTaskToTerminate.getId() + " terminated.");
				currentRuntime = firstTaskToTerminate.getRuntime();
				SimulationConstants.logger.info("Current deployment runtime: "+ deployment.runTime);
				removeEdgesOutgoingFrom(firstTaskToTerminate);
				//tmpApp.S.remove(firstTaskToTerminate);
				deployment.get(firstTaskToTerminate).undeploy(firstTaskToTerminate);;
				scheduledNodes.remove(firstTaskToTerminate);
			}
			
			ArrayList<SoftwareComponent> eligibleNodes = selectElegibleNodes();
			/*
			 * scheduledNodes is empty and deployment is not complete
			 * implies deployment not possible 
			 
			if(eligibleNodes.isEmpty())
				if(scheduledNodes.isEmpty())
					return null;
			else
				continue;
			
			while(!eligibleNodes.isEmpty())
			{
				MobileSoftwareComponent toSchedule = (MobileSoftwareComponent) eligibleNodes.get(0);
				ComputationalNode bestTarget = findTarget(deployment,toSchedule);
				int j = 1;
				while(bestTarget == null && j < eligibleNodes.size())
				{
					toSchedule = (MobileSoftwareComponent) eligibleNodes.get(j);
					bestTarget = findTarget(deployment,toSchedule);
					j++;
				}
				if(bestTarget == null && j == eligibleNodes.size())
					break;
				
				double minminruntime = toSchedule.getRuntimeOnNode(bestTarget, I);

				for(int i = 1; i < eligibleNodes.size(); i++)
				{
					MobileSoftwareComponent msc = 
							(MobileSoftwareComponent) eligibleNodes.get(i);

					ComputationalNode target = findTarget(deployment,msc);
					if(target==null)
						continue;
					if(msc.getRuntimeOnNode(target, I) < minminruntime){
						bestTarget = target;
						toSchedule = msc;
					}

				}
				deploy(deployment,toSchedule,bestTarget);
				SimulationConstants.logger.info(toSchedule.getId() + " scheduled on " + bestTarget.getId());
				setRunningTime(deployment, toSchedule, bestTarget);
				partialDeploy.put(bestTarget,toSchedule);
				scheduledNodes.add(toSchedule);
				eligibleNodes.remove(toSchedule);
			}

		}
		
		SimulationConstants.logger.info("Deployment complete: " + deployment);
		
		deployment.runTime += currentRuntime;
		return deployment;
	}
	*/

	
	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		ComputationalNode target = null;
		if(!msc.isOffloadable())
		{
			if(isValid(deployment,msc,I.getMobileDevice(msc.getUid())))
				return I.getMobileDevice(msc.getUid());
			else
				return null;
		}
		else
		{
			ArrayList<ComputationalNode> compNodes = compatibleNodes(deployment, msc);
			double currRuntime = Double.MAX_VALUE;
			for(int i = 0; i < compNodes.size(); i++)
			{
				ComputationalNode cn = compNodes.get(i);
				if(!isValid(deployment,msc,cn))
					continue;
				else
				{
					SimulationConstants.logger.info("currRuntime: "
							+currRuntime + " runtime on node " + cn.getId() + "=" + msc.getRuntimeOnNode(cn, I));
					if(msc.getRuntimeOnNode(cn, I) < currRuntime)
					{
						target = cn;
						currRuntime = msc.getRuntimeOnNode(cn, I);
					}
				}
			}
		}
		return target;
	}

}
