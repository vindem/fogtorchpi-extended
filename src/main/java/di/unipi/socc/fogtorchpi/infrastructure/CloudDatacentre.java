/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package di.unipi.socc.fogtorchpi.infrastructure;

import di.unipi.socc.fogtorchpi.application.ExactThing;
import java.util.Collection;
import java.util.List;

import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import at.ac.tuwien.ec.infrastructuremodel.energy.CPUEnergyModel;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.utils.Cost;
import di.unipi.socc.fogtorchpi.utils.Couple;
import di.unipi.socc.fogtorchpi.utils.Hardware;
import di.unipi.socc.fogtorchpi.utils.Software;
import java.util.HashMap;

/**
 *
 * @author stefano
 */
public class CloudDatacentre extends ComputationalNode {
    
    HashMap<String, Hardware> virtualMachines;
    
    public HashMap<String, Hardware> getVirtualMachines() {
		return virtualMachines;
	}

	public void setVirtualMachines(HashMap<String, Hardware> virtualMachines) {
		this.virtualMachines = virtualMachines;
	}

	public CloudDatacentre(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        virtualMachines =  new HashMap<>();
    }
    
    public CloudDatacentre(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        
        this.virtualMachines = new HashMap<>();
        
        for ( Couple<String,Double> c : vmTypes ){
            this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
        }
        
        
    }
    
    public CloudDatacentre(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes, CPUEnergyModel cpuModel){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        super.setCPUEnergyModel(cpuModel);
        
        this.virtualMachines = new HashMap<>();
        
        for ( Couple<String,Double> c : vmTypes ){
            this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
        }
        
        
    }
    
    public CloudDatacentre(String identifier, List<Couple<String, Double>> software,  double x, double y, Hardware h, List<Couple<String, Double>> vmTypes, CPUEnergyModel cpuModel, double mipsPerCore){
        super.setHardware(h);
        super.setId(identifier);
        super.setSoftware(software);
        super.setCoordinates(x,y);
        super.setKeepLight(false);
        super.setCPUEnergyModel(cpuModel);
        super.setMipsPerCore(mipsPerCore);
        
        this.virtualMachines = new HashMap<>();
        if(vmTypes != null)
        for ( Couple<String,Double> c : vmTypes )
            this.virtualMachines.put(identifier, new Hardware((String)c.getA(), (Double)(c.getB())));
        
        
        
    }

   
	@Override
	public boolean isCompatible(MobileSoftwareComponent component, MobileCloudInfrastructure i) {
		return isCompatible(component);
		
	}
	
    public boolean isCompatible(SoftwareComponent component) {
		Hardware hardwareRequest = component.getHardwareRequirements();
        Collection<Software> softwareRequest = component.getSoftwareRequirements();
        
        return super.getHardware().supports(hardwareRequest) && 
                softwareRequest.stream().noneMatch(
                        (t) -> (!super.getSoftware().containsValue(t))
                ) ;

    }

    @Override
    public void deploy(SoftwareComponent s) {
    	super.getHardware().deploy(s.getHardwareRequirements());
    }

    @Override
    public void undeploy(SoftwareComponent s) {
    	super.getHardware().undeploy(s.getHardwareRequirements());
    }
    
    @Override
    public String toString(){
        String result = "<";
        result = result + getId() + ", " + this.getSoftware()+ ", "+ this.getCoordinates()+ "\n" +
                this.virtualMachines + " " + super.getHardware();
        ;
        result += ">";
        return result; 
    }
    
    @Override
    public double computeHeuristic(SoftwareComponent s) { //, Coordinates deploymentLocation
        this.heuristic = 4;//+ 1/(deploymentLocation.distance(this.getCoordinates()));
        //System.out.println(this.getId() + " " + this.heuristic);
        if (super.getKeepLight()){
            this.heuristic = 0;
        }
        return heuristic;
    }
    
    
    @Override
    public Cost computeCost(SoftwareComponent s, Infrastructure I) {

        double cost = 0.0;
        Hardware sHardwareReqs = s.getHardwareRequirements();
        
        if (sHardwareReqs.stringDescribed && !this.virtualMachines.isEmpty()){
            cost += this.virtualMachines.get(sHardwareReqs.vmType).getMonthlyCost(s);
        } else {
            cost += this.getHardware().getMonthlyCost(s) * s.getRuntimeOnNode(this, (MobileCloudInfrastructure) I);
        }

        for (Software soft : s.getSoftwareRequirements()) {
            cost += super.getSoftware().get(soft.getName()).getCost();
        }

        for (ThingRequirement thing : s.getThingsRequirements()) {
            cost += ((ExactThing) thing).getMonthlyInvoke() * I.T.get(((ExactThing) thing).getId()).getMonthlyCost(s);
        }

        return new Cost(cost);
    }
    
}
