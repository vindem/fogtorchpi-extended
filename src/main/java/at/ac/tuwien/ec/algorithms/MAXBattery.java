package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class MAXBattery extends DAGResearch {
	
	public MAXBattery(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public MAXBattery(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		ComputationalNode target = null;
		if(!msc.isOffloadable())
		{
			if(isValid(deployment,msc,I.getMobileDevice(msc.getUid())))
				return I.getMobileDevice(msc.getUid());
			else
				return null;
		}
		else
		{
			ArrayList<ComputationalNode> compNodes = compatibleNodes(deployment,msc);
			double currBattery = Double.MIN_VALUE, tmpBattery;
			for(int i = 0; i < compNodes.size(); i++)
			{
				ComputationalNode cn = compNodes.get(i);
				if(!isValid(deployment,msc,cn))
					continue;
				else
				{
					SimulationConstants.logger.info("currRuntime: "
							+currBattery + " runtime on node " + cn.getId() + "=" + msc.getRuntimeOnNode(cn, I));
					if(cn.isMobile())
						tmpBattery = cn.getCPUEnergyModel().computeCPUEnergy(msc, cn, I);
					else
						tmpBattery = I.getMobileDevice(msc.getUid()).getNetEnergyModel().computeNETEnergy(msc, cn, I);
					if(tmpBattery > currBattery)
					{
						target = cn;
						currBattery = tmpBattery;
					}
				}
			}

			return target;						
		}

	}
	
}
