package at.ac.tuwien.ec.appmodel;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.application.ThingRequirement;
import di.unipi.socc.fogtorchpi.utils.Hardware;

public class MobileSoftwareComponent extends SoftwareComponent {

	private boolean offloadable;
	private double inData, outData, runtime = 0.0;
	private double upRank = Double.NaN;
	private double downRank = Double.NaN;
		
	
	
	public MobileSoftwareComponent(String id) {
		super(id,"",new LinkedList<String>(),new Hardware(), new ArrayList<ThingRequirement>());
		visited = false;
		// TODO Auto-generated constructor stub
	}

	public MobileSoftwareComponent(String identifier, String uid, List<String> softwareReqs, Hardware hardwareReqs, ArrayList<ThingRequirement> Theta){
		super(identifier,uid,softwareReqs,hardwareReqs,Theta);
		inData = hardwareReqs.storage;
		outData = hardwareReqs.storage;
		visited = false;
	}
	
	public void setOffloadable(boolean offloadable){
		this.offloadable = offloadable;
	}
	
	public boolean isOffloadable(){
		return offloadable;
	}

	public double getInData() {
		return inData;
	}

	public void setInData(double inData) {
		this.inData = inData;
	}

	public double getOutData() {
		return outData;
	}

	public void setOutData(double outData) {
		this.outData = outData;
	}
	
	public double getOffloadData(){
		return inData;// + (getMips() * AVERAGE_INSTRUCTION_LENGTH * 1e6);
	}

	public double getRuntime() {
		return runtime;
	}

	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}

	public boolean equals(MobileSoftwareComponent cmp){
		return cmp.getId().equals(this.getId());
	}
	
}
