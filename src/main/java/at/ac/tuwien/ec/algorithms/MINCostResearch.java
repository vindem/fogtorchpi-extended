package at.ac.tuwien.ec.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import at.ac.tuwien.ec.appmodel.MobileApplication;
import at.ac.tuwien.ec.appmodel.MobileSoftwareComponent;
import at.ac.tuwien.ec.edgeoffload.SimulationConstants;
import at.ac.tuwien.ec.infrastructuremodel.MobileCloudInfrastructure;
import di.unipi.socc.fogtorchpi.application.SoftwareComponent;
import di.unipi.socc.fogtorchpi.deployment.Deployment;
import di.unipi.socc.fogtorchpi.deployment.Search;
import di.unipi.socc.fogtorchpi.infrastructure.ComputationalNode;
import di.unipi.socc.fogtorchpi.utils.Couple;

public class MINCostResearch extends DAGResearch {
	
	public MINCostResearch(MobileApplication A, MobileCloudInfrastructure I) {
		super(A, I);
	}
	
	public MINCostResearch(MobileApplication A, MobileCloudInfrastructure I,
			HashMap<String, HashSet<String>> businessPolicies) {
		super(A, I, businessPolicies);
		// TODO Auto-generated constructor stub
	}

	protected ComputationalNode findTarget(Deployment deployment, MobileSoftwareComponent msc) {
		ComputationalNode target = null;
		if(!msc.isOffloadable())
		{
			if(isValid(deployment,msc,I.getMobileDevice(msc.getUid())))
				return I.getMobileDevice(msc.getUid());
			else
				return target;
		}
		else
		{
			ArrayList<ComputationalNode> compNodes = compatibleNodes(deployment, msc);
			double currCost = Double.MAX_VALUE;
			for(int i = 0; i < compNodes.size(); i++)
			{
				ComputationalNode cn = compNodes.get(i);
				if(!isValid(deployment,msc,cn))
					continue;
				else
				{
					SimulationConstants.logger.info("currCost: "
							+currCost + " cost on node " + cn.getId() + "=" + cn.computeCost(msc, I).getCost());
					if(cn.computeCost(msc, I).getCost() < currCost)
					{
						target = cn;
						currCost = cn.computeCost(msc, I).getCost();
					}
				}
			}

			return target;						
		}

	}

}
